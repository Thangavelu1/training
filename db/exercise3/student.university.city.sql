SELECT roll_number
      ,student.name
      ,gender
      ,student.dob
      ,student.email
      ,student.phone
      ,address
      ,college.name
      ,dept_name
      ,employee.name
  FROM student
       INNER JOIN college 
       ON college.id = student.college_id
       INNER JOIN university 
       ON university.univ_code = college.univ_code
          AND (city = 'Tiruppur' AND university_name = 'Periyar University')
       INNER JOIN college_department 
       ON college_department.cdept_id = student.cdept_id
       INNER JOIN department 
       ON department.dept_code = college_department.udept_code
       INNER JOIN employee 
       ON employee.college_id = college.id
          AND employee.cdept_id = college_department.cdept_id
         AND desig_id = '2'