SELECT university_name
      ,college.name
      ,semester
      ,sum(amount) AS collected_fees
  FROM semester_fee
       INNER JOIN student 
       ON student.id = semester_fee.stud_id
       INNER JOIN college 
       ON college.id = student.college_id
       INNER JOIN university 
       ON university.univ_code = college.univ_code
 WHERE paid_status = 'PAID'
   AND (semester = '1' OR semester = '3' OR semester = '5' OR semester = '7')
 GROUP BY semester_fee.semester, college.name, university.university_name;
SELECT university_name
      ,college.name
      ,semester
      ,sum(amount) AS uncollected_fees
  FROM semester_fee
       INNER JOIN student 
       ON student.id = semester_fee.stud_id
       INNER JOIN college 
       ON college.id = student.college_id
       INNER JOIN university 
       ON university.univ_code = college.univ_code
 WHERE paid_status = 'UNPAID'
   AND (semester = '1' OR semester = '3' OR semester = '5'OR semester = '7')
 GROUP BY semester_fee.semester, college.name, university.university_name;