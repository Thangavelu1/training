SELECT roll_number
      ,student.name
      ,gender
      ,dob
      ,email
      ,phone
      ,address
      ,college.name
      ,dept_name
  FROM student
       INNER JOIN college 
       ON college.id = student.college_id
          AND (city = 'Coimbatore' AND academic_year = '2018')
       INNER JOIN University 
       ON university.univ_code = college.univ_code
          AND college.id = student.college_id
          AND university_name = 'Anna University'
       INNER JOIN college_department 
       ON college_department.cdept_id = student.cdept_id
       INNER JOIN department 
       ON department.dept_code = college_department.udept_code;