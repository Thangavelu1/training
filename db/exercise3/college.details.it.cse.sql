SELECT code
      ,college.name
      ,city
      ,state
      ,year_opened
      ,university_name
      ,dept_name
      ,employee.name
  FROM college
       INNER JOIN university 
       ON university.univ_code = college.univ_code
       INNER JOIN department 
       ON department.univ_code = college.univ_code 
       INNER JOIN college_department 
       ON college_department.udept_code = department.dept_code 
          AND college_department.college_id = college.id
          AND (dept_name = 'IT' OR dept_name = 'CSE')
       INNER JOIN employee 
       ON employee.cdept_id = college_department.cdept_id 
          AND employee.desig_id = '2'
 ORDER BY code; 