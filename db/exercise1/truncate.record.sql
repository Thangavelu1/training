CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id      INT         NOT NULL
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL
   ,experience     INT         NOT NULL
   ,dept_id        INT         NULL);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, dept_id) 
VALUES ('1', 'thangavelu','30','cardiologist','5','1')
       ('2', 'naveen', '30', 'neurologist', '7', '2')
       ('3', 'venkatachalam', '45', 'dermatologist', '5', '3');
TRUNCATE TABLE hospital.doctor;