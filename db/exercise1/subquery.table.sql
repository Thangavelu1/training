CREATE TABLE hospital.department(
    PRIMARY KEY(dept_id)
   ,dept_id         INT         NOT NULL
   ,department_name VARCHAR(45) NOT NULL);
INSERT INTO hospital.department (dept_id, department_name)
VALUES ('1', 'CARDIOLOGY')
       ('2', 'NEUROLOGY')
       ('3', 'DERMATOLOGY')
       ('4', 'OPTHALMOLOGY')
       ('5', 'ANESTHESIOLOGY')
       ('6', 'ONCOLOGY');
CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id      INT         NOT NULL
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL
   ,experience     INT         NOT NULL
   ,dept_id        INT         NULL
   ,city           VARCHAR(45) NOT NULL
   ,salary         INT         NOT NULL
                  ,INDEX dept_id_idx (dept_id ASC) VISIBLE
                  ,CONSTRAINT dept_id
                        FOREIGN KEY(dept_id)
                        REFERENCES hospital.department(dept_id)
                        ON DELETE RESTRICT
                        ON UPDATE CASCADE);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
VALUES ('1', 'Thangavelu','30','Cardiologist','5', '1', 'coimbatore', '1000000')
       ('2', 'Naveen', '30', 'Neurologist', '5', '2' , 'tirppur', '400000')
       ('3', 'Venkatachalam', '45', 'Dermatologist', '20', '3', 'salem', '800000')
       ('4', 'Saravanan', '50', 'Opthalmologist', '25', '4', 'erode', '400000')
       ('5', 'Sushmitha', '60', 'Anesthesiologist', '35', '5', 'madurai', '500000');
       ('6', 'Sri', '35', 'Oncologist', '10', '6', 'theni', '700000');
SELECT doctor_id
      ,name
      ,age
      ,department_name
      ,specialization
      ,experience
      ,city
      ,salary
  FROM hospital.doctor, hospital.department
 WHERE hospital.doctor.dept_id = hospital.department.dept_id;
SELECT name
      ,age
      ,specialization
      ,experience
      ,city
      ,salary
  FROM hospital.doctor
 WHERE dept_id = (SELECT dept_id 
                    FROM hospital.department 
                   WHERE department_name = 'ONCOLOGY');