CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id      INT         NOT NULL
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL
   ,experience     INT         NOT NULL
   ,dept_id        INT         NULL
   ,salary         INT         NULL);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, salary, dept_id) 
VALUES ('1','thangavelu','30','cardiologist','5', '500000', '1')
       ('2', 'naveen', '30', 'neurologist', '7', '600000', '2')
       ('3', 'venkatachalam', '45', 'dermatologist', '5', '700000', '3');
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, dept_id) 
VALUES ('4', 'vijay', '50', 'oncology', '10', '4');
CREATE VIEW doctorview AS
    SELECT name
          ,salary
      FROM doctor
     WHERE salary IS NOT NULL
      WITH CHECK OPTION;
CREATE VIEW adminview AS
    SELECT doctor_id
          ,name
          ,specialization
          ,experience
          ,dept_id
      FROM doctor;
    SELECT *
      FROM doctorview;
    SELECT *
      FROM adminview;