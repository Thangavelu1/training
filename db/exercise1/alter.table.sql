CREATE TABLE hospital.doctor (doctor_id INT NOT NULL
            ,name VARCHAR(45) NOT NULL
            ,age INT NOT NULL
            ,specialization VARCHAR(45) NOT NULL
            ,experience INT NOT NULL
            ,dept_id INT NULL
            ,PRIMARY KEY (`doctor_id`));
        DESC hospital.doctor;
 ALTER TABLE hospital.doctor
  ADD COLUMN dob DATE NOT NULL AFTER name ;
        DESC hospital.doctor;
 ALTER TABLE hospital.doctor 
        DROP experience ;
        DESC hospital.doctor;