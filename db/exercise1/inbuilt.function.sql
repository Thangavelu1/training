CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id       INT         NOT NULL
   ,name            VARCHAR(45) NOT NULL
   ,age             INT         NOT NULL
   ,specialization  VARCHAR(45) NOT NULL
   ,experience      INT         NOT NULL
   ,dept_id         INT         NULL
   ,city            VARCHAR(45) NOT NULL
   ,salary          INT         NOT NULL);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, city, salary) 
VALUES ('1','Thangavelu','30','Cardiologist','5', 'coimbatore', '1000000')
       ('2', 'Naveen', '30', 'Neurologist', '5', 'tirppur', '400000')
       ('3', 'Venkatachalam', '45', 'Dermatologist', '20', 'salem', '800000')
       ('4', 'Saravanan', '50', 'Allergist', '25', 'erode', '400000')
       ('5', 'Sushmitha', '60', 'Anesthesiologist', '35', 'madurai', '500000')
       ('6', 'Sri', '35', 'Oncologist', '10', 'theni', '700000');
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
      ,salary
  FROM hospital.doctor;
SELECT MIN(salary), MIN(age)
  FROM hospital.doctor;
SELECT MAX(salary), MIN(age)
  FROM hospital.doctor;
SELECT COUNT(doctor_id)
  FROM hospital.doctor;
SELECT AVG(salary)
  FROM hospital.doctor;
SELECT SUM(salary)
  FROM hospital.doctor;
SELECT now();
      