CREATE TABLE hospital.doctor (doctor_id INT NOT NULL
            ,name VARCHAR(45) NOT NULL
            ,age INT NOT NULL
            ,specialization VARCHAR(45) NOT NULL
            ,experience INT NOT NULL
            ,dept_id INT NULL
            ,PRIMARY KEY (`doctor_id`));
        DESC hospital.doctor;
 INSERT INTO hospital.doctor (doctor_id,name,age,specialization,experience,dept_id) 
      VALUES ('1','thangavelu','30','cardiologist','5','1');
 INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, dept_id) 
      VALUES ('2', 'naveen', '30', 'neurologist', '7', '2');
 INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience, dept_id) 
      VALUES ('3', 'venkatachalam', '45', 'dermatologist', '5', '3');
  DESC TABLE hospital.doctor;
 DELETE FROM hospital.doctor
       WHERE dept_id = '2';
  DESC TABLE hospital.doctor;
 DELETE FROM hospital.doctor
       LIMIT 1;
  DESC TABLE hospital.doctor;