CREATE TABLE hospital.department(
    PRIMARY KEY(dept_id)
   ,dept_id         INT         NOT NULL
   ,department_name VARCHAR(45) NOT NULL);
 INSERT INTO hospital.department (dept_id, department_name)
      VALUES ('1', 'CARDIOLOGY')
      VALUES ('2', 'NEUROLOGY')
      VALUES ('3', 'DERMATOLOGY')
      VALUES ('4', 'OPTHALMOLOGY')
      VALUES ('5', 'ANESTHESIOLOGY')
      VALUES ('6', 'ONCOLOGY');
CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id       INT         NOT NULL
   ,name            VARCHAR(45) NOT NULL
   ,age             INT         NOT NULL
   ,specialization  VARCHAR(45) NOT NULL
   ,experience      INT         NOT NULL
   ,dept_id         INT         NULL
   ,city            VARCHAR(45) NOT NULL
                   ,INDEX dept_id_idx (dept_id ASC) VISIBLE
                   ,CONSTRAINT dept_id
                        FOREIGN KEY(dept_id)
                        REFERENCES hospital.department(dept_id)
                        ON DELETE RESTRICT
                        ON UPDATE CASCADE);
INSERT INTO hospital.doctor ( doctor_id, name, age, specialization, experience, dept_id, city) 
VALUES ( '1', 'Thangavelu', '30', 'Cardiologist', '5', '1', 'coimbatore')
       ( '2', 'Naveen', '30', 'Neurologist', '5', '2', 'tirppur')
       ( '3', 'Venkatachalam', '45', 'Dermatologist', '20', '3', 'salem')
       ( '4', 'Saravanan', '50', 'Allergist', '25', '4', 'erode')
       ( '5', 'Sushmitha', '60', 'Anesthesiologist', '35', '5', 'madurai')
       ( '6', 'Sri', '35', 'Oncologist', '10', '6', 'theni');
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE experience = '25' 
   AND specialization = 'Allergist';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE age = '30' 
    OR city = 'madurai';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE doctor_id IN ('1', '2', '3');
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE NOT age = '50';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE 's%';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE '%n';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE age LIKE '3%5';
SELECT doctor_id
      ,name
      ,age
      ,specialization
      ,experience
      ,city
  FROM hospital.doctor
 WHERE name LIKE 's_i';
SELECT name
      ,age
      ,specialization
  FROM hospital.doctor
 WHERE dept_id = ANY (SELECT dept_id 
                        FROM hospital.department 
                       WHERE dept_id = '2');