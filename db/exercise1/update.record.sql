CREATE TABLE hospital.doctor (
    PRIMARY KEY (doctor_id)
   ,doctor_id      INT         NOT NULL
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL
   ,experience     INT         NOT NULL);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization, experience) 
VALUES ('1', 'thangavelu', '30', 'cardiologist', '5')
       ('2', 'naveen', '30', 'neurologist', '7')
       ('3', 'venkatachalam', '45', 'dermatologist', '5');
UPDATE hospital.doctor
   SET age = '40'
 WHERE doctor_id = 1;
UPDATE hospital.doctor
   SET specialization = 'doctor';
SELECT name
      ,age
      ,specialization
      ,experience
  FROM hospital.doctor;