CREATE TABLE hospital.department(
    PRIMARY KEY(dept_id)
   ,dept_id INT         NOT NULL
   ,name    VARCHAR(45) NOT NULL);
INSERT INTO hospital.department (dept_id, name)
VALUES ('1', 'CARDIOLOGY')
       ('2', 'NEUROLOGY')
       ('3', 'DERMATOLOGY')
       ('4', 'OPTHALMOLOGY')
       ('5', 'ANESTHESIOLOGY');
       ('6', 'ONCOLOGY');
CREATE TABLE hospital.additional_details(
    PRIMARY KEY(id)
   ,id         INT         NOT NULL
   ,experience INT         NULL
   ,salary     INT         NOT NULL
   ,city       VARCHAR(45) NOT NULL);
INSERT INTO hospital.additional_details (id, experience, salary, city)
VALUES ( '1', '5', '1000000', 'coimbatore')
       ( '2', '5', '400000', 'tiruppur')
       ( '3', '20', '800000', 'salem')
       ( '4', '25', '400000', 'erode')
       ( '5', '35', '500000', 'madurai')
       ( '6', '10', '700000', 'theni');
CREATE TABLE hospital.doctor (
    PRIMARY KEY(doctor_id)
   ,doctor_id      INT         AUTO_INCREMENT
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialisation VARCHAR(45) NOT NULL
   ,dept_id        INT         NOT NULL
   ,id             INT         NULL
                  ,INDEX dept_id_idx (dept_id ASC) VISIBLE
                  ,CONSTRAINT dept_id
                        FOREIGN KEY(dept_id)
                        REFERENCES hospital.department(dept_id)
                        ON DELETE RESTRICT
                        ON UPDATE CASCADE
                  ,INDEX id_idx (id ASC) VISIBLE
                  ,CONSTRAINT id
                        FOREIGN KEY(id)
                        REFERENCES hospital.additional_details(id)
                        ON DELETE RESTRICT
                        ON UPDATE CASCADE);
INSERT INTO hospital.doctor (doctor_id, name, age, specialisation, dept_id, id)
VALUES ('1','Thangavelu', '30', 'Cardiologist', '1', '1')
       ('2', 'Naveen', '30', 'Neurologist', '2', '2')
       ('3', 'Venkatachalam', '45', 'Dermatologist', '3', '3')
       ('4', 'Saravanan', '50', 'Allergist', '4', '4')
       ('5', 'Sushmitha', '60', 'Anesthesiologist', '5', '5')
       ('6', 'Sri', '35', 'Oncologist', '6', '6')
       ('7', 'Praveen', '40', 'Dermatologist', '3', '3')
       ('8', 'Karthi', '35', 'Cardiologist', '1', '2')
       ('9', 'Surya', '40', 'Neurologist', '2', '6')
       ('10', 'Harish', '45', 'Oncologist', '6', '2');
SELECT doctor.name
      ,doctor.age
      ,doctor.specialisation
      ,department.name
      ,additional_details.experience
      ,additional_details.salary
      ,additional_details.city
  FROM hospital.doctor
      ,hospital.department
      ,hospital.additional_details
       CROSS JOIN hospital.department
       CROSS JOIN hospital.additional_details;
SELECT doctor.name
      ,doctor.age
      ,doctor.specialisation
      ,department.name
      ,additional_details.experience
      ,additional_details.salary
      ,additional_details.city
  FROM hospital.doctor
      ,hospital.department
      ,hospital.additional_details
       INNER JOIN hospital.department
       INNER JOIN hospital.additional_details;
SELECT doctor.name
      ,doctor.age
      ,doctor.specialisation
      ,department.name
      ,additional_details.experience
      ,additional_details.salary
      ,additional_details.city
  FROM hospital.doctor 
      ,hospital.department
      ,hospital.additional_details
       LEFT JOIN hospital.department
       ON hospital.doctor.dept_id = hospital.department.dept_id
       LEFT JOIN hospital.additional_details
       ON hospital.doctor.id = hospital.additional_details.id;
SELECT doctor.name
      ,doctor.age
      ,doctor.specialisation
      ,department.name
      ,additional_details.experience
      ,additional_details.salary
      ,additional_details.city
  FROM hospital.doctor
      ,hospital.department
      ,hospital.additional_details
       RIGHT JOIN hospital.department
       ON hospital.doctor.dept_id = hospital.department.dept_id
       RIGHT JOIN hospital.additional_details
       ON hospital.doctor.id = hospital.additional_details.id;