CREATE TABLE hospital.doctor (
    PRIMARY KEY(doctor_id)
   ,doctor_id      INT         AUTO_INCREMENT
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL);
INSERT INTO hospital.doctor (doctor_id, name, age, specialization)
VALUES ('1', 'Thangavelu','30','Cardiologist')
       ('2', 'Naveen', '30', 'Neurologist')
       ('3', 'Venkatachalam', '45', 'Dermatologist');
CREATE TABLE hospital.doctor1 (
    PRIMARY KEY(doctor_id)
   ,doctor_id      INT         AUTO_INCREMENT
   ,name           VARCHAR(45) NOT NULL
   ,age            INT         NOT NULL
   ,specialization VARCHAR(45) NOT NULL);
 INSERT INTO hospital.doctor1 (doctor_id, name, age, specialization) 
 VALUES ('4', 'Saravanan', '50', 'Opthalmologist')
        ('5', 'Sushmitha', '60', 'Anesthesiologist')
        ('6', 'Sri', '35', 'Oncologist')
        ('2', 'Naveen', '30', 'Neurologist');
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor
 UNION
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor1;
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor
 UNION ALL
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor1;
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor
 UNION DISTINCT
SELECT doctor_id
      ,name
      ,age
      ,specialization
  FROM hospital.doctor1;