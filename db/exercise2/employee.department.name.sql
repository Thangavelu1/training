SELECT employee.first_name 
      ,employee.annual_salary
      ,department.name 
  FROM db.employee 
      ,db.department 
 WHERE employee.dept_id=department.dept_id;