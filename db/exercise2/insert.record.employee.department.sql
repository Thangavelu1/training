INSERT INTO industry.department (dept_id, name)
VALUES ('1', 'ITDesk')
       ('2', 'Finance')
       ('3', 'Engineering')
       ('4', 'HR')
       ('5', 'Recruitment')
       ('6', 'Facility');
INSERT INTO industry.employee (id, first_name, surname, dob, date_of_joining, annual_salary, dept_id)
VALUES ('1', 'Thangavelu', 'R', '2001-05-14', '2020-02-19', '500000', '1')
       ('2', 'Naveen', 'A', '2001-06-14', '2019-08-02', '400000', '1 ')
       ('3', 'Venkatachalam', 'K', '2000-02-18', '2019-08-08', '500000', '1')
       ('4', 'Sushmitha', 'S P', '2000-09-17', '2020-01-19', '140000', '1')
       ('5', 'Praveen', '1999-12-12', '2020-02-19', '350000', '1')
       ('6', 'Kathir', '1995-07-04', '2019-08-02', '450000', '6')
       ('7', 'Kavin', '2000-08-09', '2020-01-19', '500000', '2')
       ('8', 'Vignesh', '2002-07-12', '2020-02-19', '900000', '2')
       ('9', 'Vishnu', 'K', '2000-02-15', '2019-08-08', '600000', '2')
       ('10', 'Sharook', 'R', '2000-04-15', '2020-01-19', '400000', '2')
       ('11', 'Aswin', '2001-05-14', '2019-08-02', '750000', '3')
       ('12', 'Aathi', '2003-12-12', '2020-02-19', '800000', '3')
       ('13', 'Arun', 'L', '1999-02-09', '2019-08-08', '140000', '3')
       ('14', 'Surya', '2001-04-05', '2019-08-02', '500000', '3')
       ('15', 'Gowtham', '2000-08-08', '2019-05-04', '550000', '3')
       ('16', 'Deepak', 'S', '2000-06-06', '2020-01-19', '650000', '4')
       ('17', 'Karthi', '2001-01-01', '2019-08-02', '750000', '4')
       ('18', 'Shyam', '2000-01-15', '2019-08-08', '120000', '4')
       ('19', 'Saran', '2000-01-14', '2019-05-04', '800000', '4')
       ('20', 'Sarath', '1999-02-13', '2020-01-19', '850000', '4')
       ('21', 'swetha', '2000-08-08', '2019-05-04', '452000', '5')
       ('22', 'Aravind', '2020-01-18', '2020-01-19', '450000', '5')
       ('23', 'Mahesh', '2001-04-15', '2019-08-08', '130000', '5')
       ('24', 'Kevin', '2001-02-08', '2019-08-02', '500000', '5')
       ('25', 'Prakash', '1998-09-09', '2019-05-04', '650000', '5')
       ('26', 'vinayak', '1995-05-05', '2019-08-08', '700000', '6')
       ('27', 'Pranav', '2001-01-01', '2020-02-19', '850000', '6')
       ('28', 'Ram', '2002-12-13', '2019-08-02', '400000', '6')
       ('29', 'Vijay', '2000-12-03', '2019-08-08', '500000', '6')
       ('30', 'Shiva', '2004-08-19', '2020-02-19', '850000', '6');