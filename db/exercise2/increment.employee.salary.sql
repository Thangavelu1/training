UPDATE db.employee
   SET annual_salary = ((annual_salary*10)/100) + annual_salary;
SELECT employee.first_name 
      ,employee.annual_salary
      ,department.name 
  FROM db.employee 
      ,db.department 
 WHERE employee.dept_id=department.dept_id;        