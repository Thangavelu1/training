SELECT employee.first_name
      ,department.name 
	  ,employee.area
  FROM employee employee
      ,department department 
 WHERE employee.department_id=department.id 
  AND (employee.area IN('coimbatore') AND department.name IN('Recruitment'));