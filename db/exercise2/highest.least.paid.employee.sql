SELECT name 
      ,min(annual_salary) AS least_paid
      ,max(annual_salary) AS highest_paid
  FROM db.employee 
      ,db.department
 WHERE department.dept_id = employee.dept_id
 GROUP BY employee.dept_id;