CREATE TABLE employee(
    PRIMARY KEY (emp_id) 
   ,emp_id        VARCHAR(5)  NOT NULL
   ,first_name    VARCHAR(20) NULL
   ,dob           VARCHAR(25) NOT NULL
   ,annual_salary INT         NOT NULL
   ,emp_supv      VARCHAR(5)  NULL
                 ,FOREIGN KEY(emp_supv)REFERENCES employee(emp_id));
INSERT INTO employee(emp_id,first_name ,dob,annual_salary) 
VALUES (1,'naveen','2/02/2001',60000000);
INSERT INTO employee(emp_id,first_name ,dob,annual_salary,emp_supv)
VALUES (2,'kavin','4/05/2001',77787870,1)
       (3,'bhuvan','19/03/2001',6545445,1)
	   (4,'sai','22/06/2001',645454545,2)
	   (5,'praveen','4/09/2001',654545421,2);
      SELECT emp_id       
            ,first_name   
            ,dob          
            ,annual_salary
            ,emp_supv     
        FROM employee;
      SELECT a.emp_id     AS "Emp_ID"
		    ,a.first_name AS "Employee Name"
		    ,b.emp_id     AS "Supervisor ID"
		    ,b.first_name AS "Supervisor Name"
        FROM employee a, employee b
       WHERE a.emp_supv = b.emp_id;