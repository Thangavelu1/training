CREATE SCHEMA data;
 CREATE TABLE data.department (dept_id INT NOT NULL
             ,name VARCHAR(45) NULL
             ,PRIMARY KEY (dept_id));
 CREATE TABLE data.employee (id INT NOT NULL
             ,first_name VARCHAR(45) NOT NULL
             ,surname VARCHAR(45) NULL
             ,dob DATE NOT NULL
             ,date_of_joining DATE NOT NULL
             ,annual_salary INT NOT NULL
             ,dept_id INT NULL
             ,PRIMARY KEY (id)
             ,INDEX id_idx (dept_id ASC) VISIBLE
             ,CONSTRAINT id
                 FOREIGN KEY (dept_id)
                 REFERENCES data.department (dept_id)
                 ON DELETE NO ACTION
                 ON UPDATE NO ACTION);