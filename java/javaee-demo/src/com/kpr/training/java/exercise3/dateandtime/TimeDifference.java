/*
Requirement:
    To find the difference between the times using timer class.
    
Entity:
    TimeDifference

Function Declaration:
    public static void main(String[] args)
    public static void countFunction(long time)
    
Jobs to be done:
    1) Declare the long variable startTime which gets the System time in milliseconds.
           1.1) Print that starting time.
    2) for each milliseconds starting from 0 ms ending up to 100 ms 
           2.1) print that milliseconds.
    3) Now get the current system timing in millisecond and store it in the endTime.
    4) Print the difference between startTime and endTime.
    
Pseudo code:

public class TimeDifference {
	
	public static void main(String[] args) {
		
		long initialTime = System.currentTimeMillis();
        System.out.println("Initial time : " + initialTime + "ms");
        
        for (int time = 0; time < 100 ; time++) {
        	
            System.out.print(time + " ");
        }
        
        long finalTime = System.currentTimeMillis();
        System.out.println("\nFinal time : " + finalTime + " ms");
        
        System.out.println("The difference between the time is: " + (finalTime - initialTime) + " ms");
	}
}
*/

package com.kpr.training.java.exercise3.dateandtime;

public class TimeDifference {
	
	public static void main(String[] args) {
		
		long initialTime = System.currentTimeMillis();
        System.out.println("Initial time : " + initialTime + "ms");
        
        for (int time = 0; time < 100 ; time++) {
        	
            System.out.print(time + " ");
        }
        
        long finalTime = System.currentTimeMillis();
        System.out.println("\nFinal time : " + finalTime + " ms");
        
        System.out.println("The difference between the time is: " + (finalTime - initialTime) + " ms");
	}
}
