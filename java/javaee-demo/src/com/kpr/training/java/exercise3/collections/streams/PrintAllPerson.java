/*
Requirement:
    Print all the persons in the roster using java.util.Stream<T>#forEach 
    
Entity:
    PrintAllPerson
    Person
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object as roster for the method createRoster().
    2) Print all the person using for each.
    
Pseudo code:
 	public class PrintAllPerson {

		public static void main(String[] args) {
		
			List<Person> list = Person.createRoster();
			Stream<Person> stream = list.stream();
			// Print the person details using for each loop.  
		}
	}
*/

package com.kpr.training.java.exercise3.collections.streams;

import java.util.List;
import java.util.stream.Stream;

public class PrintAllPerson {
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
		
		Stream<Person> stream = roster.stream();
		
		stream.forEach(person -> 
			System.out.println("Name : " + person.name +
							   "\tBirthday : " + person.birthday +
							   "\tGender : " + person.gender +
							   "\tEmail : " +  person.emailAddress));
	}
}
