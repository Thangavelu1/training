/*
Requirement:
    To write some String content using OutputStream
   
Entity:
    OutputStreamDemo
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a reference for FileOutputStream with file as constructor argument.
    2) Get the content to be wrote in the file.
    3) Convert the given string into input stream.
    4) Write the converted content to a file.
    5) Close the created output stream.

Pseudo code:
public class OutputStreamDemo {

    public static void main(String[] args) throws IOException {
    	
        OutputStream outputStream = new FileOutputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\putData.txt");
        
        String content = "The Moon is an astronomical body orbiting Earth and is the planet's only natural satellite" +
    			         " \nThe Moon is thought to have formed about 4.51 billion years ago, not long after Earth" + 
    			         " \nAfter the Sun, the Moon is the second-brightest celestial object regularly "
    			         + "visible in Earth's sky." +
    			         " \nthe Moon covers the Sun nearly precisely during a total solar eclipse" ;
        
        byte[] inputStream = content.getBytes();
        
        outputStream.write(inputStream);
        outputStream.close();
        
        System.out.println("Content has been written successfully");
    }
}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {


    public static void main(String[] args) throws IOException {
    	
        OutputStream outputStream = new FileOutputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\putData.txt");
        
        String content = "The Moon is an astronomical body orbiting Earth and is the planet's only natural satellite" +
    			         " \nThe Moon is thought to have formed about 4.51 billion years ago, not long after Earth" + 
    			         " \nAfter the Sun, the Moon is the second-brightest celestial object regularly "
    			         + "visible in Earth's sky." +
    			         " \nthe Moon covers the Sun nearly precisely during a total solar eclipse" ;
        
        byte[] inputStream = content.getBytes();
        
        outputStream.write(inputStream);
        outputStream.close();
        
        System.out.println("Content has been written successfully");
    }
}
