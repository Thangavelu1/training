/*
Requirement:
	Consider a following code snippet:
            List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API
        
Entity:
	RandomNumbers
	Person
	
Function Declaration:
	public static void main(String[] args) {}
	
Jobs To Be Done:
	1) Create an array of Integer and store the values 1, 6, 10, 25, 78.
	2) Create a generic list of Integer and store the array values to the list.
	3) Add the numbers in the array using stream and store it in the variable sum.
	4) Get the minimum value using stream and store it in min using Optional class
		4.1) Print the minimum value.
	5) Get the maximum value using stream and store it in min using Optional class
		5.1) Print the maximum value.
	
Pseudo code:
	public class RandomNumbers {
		
		public static void main(String[] args) {
			
			Integer[] array = {1, 6, 10, 25, 78};
			List<Integer> randomNumbers = Arrays.asList(array);
			
		    int sum = randomNumbers.stream().reduce(0, (Value1, Value2) -> Value1 + Value2);
		    System.out.println(sum);
		    
		    Optional<Integer> min = randomNumbers.parallelStream().min((value1, value2) -> {
		    	return value1.compareTo(value2);
		    });
		    System.out.println(min.get());
		    
		    Optional<Integer> max = randomNumbers.parallelStream().max((value1, value2) -> {
		    	return value1.compareTo(value2);
		    });
		    System.out.println(max.get());	    
		}
	}
 */

package com.kpr.training.java.exercise3.collections.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RandomNumbers {
	
	public static void main(String[] args) {
		
		Integer[] array = {1, 6, 10, 25, 78};
		List<Integer> randomNumbers = Arrays.asList(array);
		
	    int sum = randomNumbers.stream().reduce(0, (Value1, Value2) -> Value1 + Value2);
	    System.out.println(sum);
	    
	    Optional<Integer> min = randomNumbers.parallelStream().min((value1, value2) -> {
	    	return value1.compareTo(value2);
	    });
	    System.out.println(min.get());
	    
	    Optional<Integer> max = randomNumbers.parallelStream().max((value1, value2) -> {
	    	return value1.compareTo(value2);
	    });
	    System.out.println(max.get());	    
	}
}
