/*
Requirement:
    To print the list of numbers.
    
Entity:
    ListOfNumbers
    
Function declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object for Integer array with list of numbers.
    2) Iterate the array using for loop
    3) Print the list of numbers in the array
    	3.1) Catch the exception "ArrayIndexOutOfBoundsException" if it is thrown.
    	3.2) Catch the exception if any other exception is thrown.

Pseudo code:

	public class ListOfNumbers {
		
		public static void main(String[] args) {
			
	        int[] array = new int[] {1,2,3,0,5};
	        
	        // Print a statement "Printing elements"
	        
			for(int i=0; i<array.length+1; i++) {
				
			    try {
			    	
				    // Print the list of numbers in the array
				}
				
			    // Catch the exception "ArrayIndexOutOfBoundsException" if it is thrown
			  
			    // Catch the exception if any other exception is thrown
			}
		}
	}
 */

package com.kpr.training.java.exercise3.exceptionhandling;

public class ListOfNumbers {
	
	public static void main(String[] args) {
		
        int[] array = new int[] {1,2,3,0,5};
        
        System.out.println("Printing elements");
        
		for(int i=0; i<array.length+1; i++) {
			
		    try {
		    	
			    System.out.println(array[i]);
			}
		    catch(ArrayIndexOutOfBoundsException e) {
		    	
				System.out.println("Array size exceeded");
		    }   
		    catch(Exception e) {
		    	
				System.out.println("Exception handled");
		    }
		}
	}
}
