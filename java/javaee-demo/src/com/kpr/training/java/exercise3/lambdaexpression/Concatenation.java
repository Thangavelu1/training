/* 
Requirement:
    write a Lambda expression program with a single method interface
    To concatenate two strings.
    
Entity:
    Concatenation
    LambdaConcat
    
Function Declaration:
    public static void main(String[] args) {}
    public String concatString(String word , String word1)
    
Jobs To Be Done:
    1) Create an interface LambdaConcat and declare a single method LambdaConcat of two parameters.
    2) In the main method lambda expression is used to concatenate the two strings
    3) Print the concatenated two strings.
    
Pseudo code:
	interface LambdaConcat {
		
		public String concatString(String word , String word1);
	
	}
	
	public class Concatenation {
		
		public static void main(String[] args) {
			
			LambdaConcat lambdaConcat = (string1, string2) -> {
				
				return string1 + string2 ;
			};
			
			System.out.println(lambdaConcat.concatString("hi ", "Aravind"));
		}
	}
*/

package com.kpr.training.java.exercise3.lambdaexpression;

interface LambdaConcat {
	
	public String concatString(String word , String word1);

}

public class Concatenation {
	
	public static void main(String[] args) {
		
		LambdaConcat lambdaConcat = (string1, string2) -> {
			
			return string1 + string2 ;
		};
		
		System.out.println(lambdaConcat.concatString("hi ", "Aravind"));
	}
}
