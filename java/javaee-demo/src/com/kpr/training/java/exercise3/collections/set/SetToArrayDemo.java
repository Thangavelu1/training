/*
Requirement:
    demonstrate linked hash set to array() method in java
    
Entity:
    SetToArrayDemo
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1.Create object for LinkedHashSet
    2.Add elements to the linkedHashSet
    3.copy the elements of linkedHashSet to the new Array.
    4.print the elements of array.
    
Pseudo code:
    public class SetToArrayDemo {
        public static void main(String[] args) {
            Set<String> linkedHashSet = new LinkedHashSet<String>();
            //Add elements to the linkedHashHashset
            //create new array and copy elements
            //print the array using for loop
        }
    }
 */

package com.kpr.training.java.exercise3.collections.set;

import java.util.LinkedHashSet;
import java.util.Set;

public class SetToArrayDemo {
	
	public static void main(String[] args) {
		
		Set<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("animal");
		linkedHashSet.add("bat");
		linkedHashSet.add("cat");
		linkedHashSet.add("dog");
		linkedHashSet.add("elephant");
		
		System.out.println(linkedHashSet);
		
		Object[] array = linkedHashSet.toArray();
		
		for(int i=0 ;i<array.length ;i++) {
			System.out.print(array[i] + " ");
		}
	}
}
