/*
Requirement:
    demonstrate program explaining basic add and traversal operation of linked hash set
    
Entity:
    LinkedSetDemo
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1) Create object for LinkedHashSet
    2) Add elements to the linkedHashSet
    3) Traverse or Iterate the elements of linkedHashSet
    	3.1) Using iterator
    	3.2) Using for each loop
    
Pseudo code:
    public class SetDemo {
        public static void main(String[] args) {
            Set<String> linkedHashSet = new LinkedHashSet<String>();
            //Iterating the elements in the linkedHashHashset using iterator and for each loop
        }
    }
*/

package com.kpr.training.java.exercise3.collections.set;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetDemo {
	
	public static void main(String[] args) {
		
		Set<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("animal");
		linkedHashSet.add("bat");
		linkedHashSet.add("cat");
		linkedHashSet.add("dog");
		linkedHashSet.add("elephant");
		
		System.out.println(linkedHashSet);
		
		Iterator<String> iterator = linkedHashSet.iterator();
		
		while(iterator.hasNext()) {
			String element = iterator.next();
			System.out.println(element);
		}
		
		for(String str : linkedHashSet) {
			System.out.println(str);
		}
	}
}
