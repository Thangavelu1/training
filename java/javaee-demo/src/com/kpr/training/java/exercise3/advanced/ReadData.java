/*
Requirement: 
	Reading a file using Reader.

Entity:
	ReadData

Function Declaration:
	public static void main(String[] args) {}

Jobs To Be Done:
	1) Create a reference for FileReader with file as constructor argument.
   	2) Till the end of the file
        2.1) Read the content of the file.
        2.2) Print the content of the file.
   	3) Close the created input stream.

PseudoCode:
	public class ReadData {
		
		public static void main(String[] args) throws Exception {
			
			Reader reader = new FileReader("C:\\1dev\\database\\training\\java\\javaee-demo\\"
					+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
					
			int charater;
			
			while ((charater = reader.read()) != -1) {
				
				System.out.print((char) charater);
			}
			reader.close();
		}
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.FileReader;
import java.io.Reader;

public class ReadData {
	
	public static void main(String[] args) throws Exception {
		
		Reader reader = new FileReader("C:\\1dev\\database\\training\\java\\javaee-demo\\"
				+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
		
		int charater;
		
		while ((charater = reader.read()) != -1) {
			
			System.out.print((char) charater);
		}
		reader.close();
	}
}
