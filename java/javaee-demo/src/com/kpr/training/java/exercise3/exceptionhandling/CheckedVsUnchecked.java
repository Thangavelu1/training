/*
Requirement:
    To compare checked and unchecked exception.
    
Entity:
	CheckedVsUnchecked
	   
Function Declaration:
    public static void main(String[] args) {}
    printSum(int[] array);
    static void printSum(int[] array) throws NoSuchMethodException {}
    
Jobs To Be Done:
    1) Create a new int array and store the values in the variable array.
    2) Create a method printSum() and pass the array which throw new exception.
    3) Invoke the method printSum()
    	3.1) throw new exception
    4) Divide the array value by zero
    	4.1) throw an exception
    5) Call the 5th index value from array
    	5.1) throw an exception
    	
Pseudo code:
	public class CheckedVsUnchecked {
	
		Checked Exception :
		// Create a method printSum()
	
	    public static void main(String[] args) {
			
			int[] array=new int[] {1,2,3,4,5};
			
			try {
			
				printSum(array);
			}
			// Catch the exception
			 
			Unchecked Exception :
			try {
				
				System.out.println(array[0]/0);
			}
			// Catch the exception
			
			try {
			
				System.out.println(array[5]);
			}
			// Catch the exception
		}
	}
 */

package com.kpr.training.java.exercise3.exceptionhandling;

public class CheckedVsUnchecked {
	
	// Checked Exception
	static void printSum(int[] array) throws NoSuchMethodException {
			
		throw new NoSuchMethodException();
	}
	
    public static void main(String[] args) {
		
		int[] array=new int[] {1,2,3,4,5};
		
		try {
			
			printSum(array);
		}
		catch(NoSuchMethodException e) {
			
			System.out.println("No such method");
		}
		// Unchecked Exception
		try {
			
			System.out.println(array[0]/0);
		}
		catch(ArithmeticException e) {
			
			System.out.println("divided by zero");
		}
		try {
		
			System.out.println(array[5]);
		}
		catch(ArrayIndexOutOfBoundsException e) {
			
			System.out.println("Array is out of Bound");
		}
	}
}
