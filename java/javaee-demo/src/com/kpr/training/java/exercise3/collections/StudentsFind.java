/*
Requirement:
    LIST CONTAINS 10 STUDENT NAMES
    krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
    display only names starting with 'A'.
  
Entity:
    StudentsFind
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an Linkedlist and name the object as list.
    2) Add the elements into the list.
    3) The list the values are converted into uppercase.
    4) Using for each loop and if condition the names are startswith "A" prints the name.
    
Pseudo code:
	public class StudentsFind {
		public static void main(String[] args) {

			List<String> list = new LinkedList<>();
			//add elements to the list
			//print the names that startswith "A"
		}
	}
*/ 
    	
package com.kpr.training.java.exercise3.collections;

import java.util.List;
import java.util.LinkedList;


public class StudentsFind {

    public static void main(String[] args) {
    	
    	List<String> list = new LinkedList<>();
    	
        list.add("krishnan");
        list.add("abishek");
        list.add("arun");
        list.add("vignesh");
        list.add("kiruthiga");
        list.add("murugan");
        list.add("adhithya");
        list.add("balaji");
        list.add("vicky");
        list.add("priya");
       
        list.replaceAll(s -> s.toUpperCase());
        System.out.println("list : " + list);      
        
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}
