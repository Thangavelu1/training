/*
Requirement: 
	Reverse List Using Stack with minimum 7 elements in list.
	
Entity:
	StackReverseList
	
Function Declaration:
	public static void main(String[] args) {}
	
Jobs To Be Done:
    1) Create an arrayList as list and type Integer.
    2) Add the elements to the list.
    3) Print the list using for each loop.
    4) Create a stack as type Integer.
    5) Push the elements to the stack.
    6) Create another arrayList as list1 and type Integer. 
    7) Elements in the list are popped and added to the list1.
    8) Reverse list is stored in list1.
    9) Print the list1.
    
Pseudo code:
	public class StackReverseList {
	
		public static void main(String[] args) {
	      
			ArrayList<Integer> list = new ArrayList<Integer>();
			
			// Add elements to the list using for loop
			
			list.forEach((number) -> System.out.print(number + " "));
			
			Stack<Integer> stack = new Stack<Integer>();
			
			// Push elements to the stack using for loop
			
			System.out.println();
			
			ArrayList<Integer> list1 = new ArrayList<Integer>();
			
			// Add the pop elements from stack and add to the list1
			
			System.out.println("Reversed list");
	        // Print the reverse list using for each loop
		}
	}
 */

package com.kpr.training.java.exercise3.collections.stack;

import java.util.ArrayList;
import java.util.Stack;

public class StackReverseList {

	public static void main(String[] args) {
      
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int number = 1 ;number < 8 ;number++) {
			
			list.add(number);
		}
		list.forEach((number) -> System.out.print(number + " "));
		
		Stack<Integer> stack = new Stack<Integer>();
		
		for(int number = 0 ;number < 7 ;number++) {
			
			stack.push(list.get(number));
		}
		System.out.println();
		
		ArrayList<Integer> list1 = new ArrayList<Integer>();
		
		for(int number = 0 ;number < 7 ;number++) {
			
			list1.add(stack.pop());
		}
		
		System.out.println("Reversed list");
        list1.forEach((number) -> System.out.print(number + " "));
	}
}