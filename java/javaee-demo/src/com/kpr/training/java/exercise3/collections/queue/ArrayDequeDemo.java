/*
Requirement:
    Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
    pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue .
    
Entity:
    ArrayDequeDemo
    
Function:
    public static void main(String[] args) {}
    
Jobs to be Done:
    1) Create an object for ArrayDeque as deque.
    2) Add the elements to the deque.
    3) Add the first element to the deque using addFirst() method and then print the deque.
    4) Add the last element to the deque using addLast() method and then print the deque.
    5) Removing the first and last element in the deque , then print deque.
    6) print the first and last peek element by using "peekFirst()" and "peekLast()" method.
    7) print the first and last poll element by using "pollFirst" and "pollLast" method.
    
Pseudo code:
	public class ArrayDequeDemo {
		public static void main(String[] args) {
			ArrayDeque<Integer> deque = new ArrayDeque<>();
			//add elements to the deque
			deque.addFirst();
			deque.addLast();        
			deque.removeFirst();    
			deque.removeLast();
			deque.peekFirst();
			deque.peekLast();
			deque.pollFirst();
			deque.pollLast();
			//print the results
		}
	}
*/


package com.kpr.training.java.exercise3.collections.queue;

import java.util.ArrayDeque;

public class ArrayDequeDemo {
	
    public static void main(String[] args) {
    	
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        
        deque.add(10);
        deque.add(20);
        deque.add(30);
        deque.add(40);
        deque.add(50);
        deque.add(60);
        deque.add(70);
        deque.add(80);
        
        System.out.println("The ArrayDeque : " + deque);
        
        deque.addFirst(1000);
        System.out.println("After adding the first element : " + deque);
        
        deque.addLast(8000);
        System.out.println("After adding the last element : " + deque);
        
        deque.removeFirst();
        System.out.println("After removing the first element : " + deque);
        
        deque.removeLast();
        System.out.println("After removing the first element : " + deque);
        
        System.out.println("The first peek element : " + deque.peekFirst());
        
        System.out.println("The last  peek element : " + deque.peekLast());
        
        System.out.println("The first poll element : " + deque.pollFirst());
               
        System.out.println("The last poll element : " + deque.pollLast());
    }
}