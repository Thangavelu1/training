/* 
Requirement:
    To Write a program to print employees name list by implementing iterable interface.
    
Entity:
    IterableInterface
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a class MyIterable that implements the Iterable.
    2) Declare a variable list of type List<A>.
    3) Create a method MyIterable and get the array values and store it in the variable list.
    4) Create a method iterator() to iterate the elements from the list.
    5) Create an String Array as employeeName and store the values.
    6) Create the object for MyIterable class and pass the parameter employeeName.
    7) Print the values using for each loop.
    
Pseudo code:
	class MyIterable<L> implements Iterable<L> {
		 
	    public List<L> list;
	
	    public MyIterable(L[] a) {
	
	        list = Arrays.asList(a);
	    }
	
	    public Iterator<L> iterator() {
	
	        return list.iterator();
	    }
	}
	
	public class IterableInterface {
		
		public static void main(String[] args) {
			 
	        String[] employeeName = {"Aswin", "Banu", "Chandru", "Dinesh", "Eniyan"};
	        
	        MyIterable<String> myList = new MyIterable<String>(employeeName);
	        
	        System.out.println("Employees Name :");
	        
	        for (String i : myList) {
	        	
	            System.out.println(i);
	        }
	    }
	}	
*/

package com.kpr.training.java.exercise3.generics;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<L> implements Iterable<L> {
	 
    public List<L> list;

    public MyIterable(L[] a) {

        list = Arrays.asList(a);
    }

    public Iterator<L> iterator() {

        return list.iterator();
    }
}

public class IterableInterface {
	
	public static void main(String[] args) {
		 
        String[] employeeName = {"Aswin", "Banu", "Chandru", "Dinesh", "Eniyan"};
        
        MyIterable<String> myList = new MyIterable<String>(employeeName);
        
        System.out.println("Employees Name :");
        
        for (String i : myList) {
        	
            System.out.println(i);
        }
    }
}
