/*
Requirement:
    java program to demonstrate adding elements, displaying, removing, and iterating in hash set
    
Entity:
    SetDemo
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1) Create object for HashSet.
    2) Add elements to the hashSet.
    3) Iterate the elements in the hashSet.
    4) Display the elements in the hashSet.
    5) Remove the elements in the hashSet.
    
Pseudo code:
    public class SetDemo {
        public static void main(String[] args) {
            //Creating a new HashSet
            //Adding elements to the Hashset
            //Iterating the elements in the Hashset
            //removing the elements in the Hashset
        }
    }
*/ 

package com.kpr.training.java.exercise3.collections.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		
		Set<Integer> hashSet = new HashSet<Integer>();
		
		hashSet.add(10);
		hashSet.add(20);
		hashSet.add(30);
		hashSet.add(40);
		hashSet.add(50);
		
		System.out.println(hashSet);
		
		Iterator<Integer> iterator = hashSet.iterator();
		
		while(iterator.hasNext()) {
			Integer element = iterator.next();
			System.out.println(element);
		}
		
		hashSet.remove(10);
		hashSet.remove(30);
		hashSet.remove(50);
		
		System.out.println(hashSet);
	}
}
