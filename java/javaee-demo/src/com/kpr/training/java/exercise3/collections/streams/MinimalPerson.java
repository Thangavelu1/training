/*
Requirement:
    To print minimal person with name and email address from the Person class 
    	using java.util.Stream<T> API by referring Person.java
 
Entity:
    MinimalPerson
    Person

Function Declaration:
    public static void main(String[] args) {}

Jobs To Be Done:
    1) Create an object as roster for the method createRoster().
    2) Create a ArrayList of type String as name
    3) Create a ArrayList of type String as mailId
    4) Create minimalName of type String and get the minimalName in the ArrayList name.
    5) Create minimalMailId of type String and get the minimalMailId in the ArrayList mailId
    6) Print the minimalName and minimalMailId.
    
Pseudo code:
 
public class MinimalPerson {
    
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>(); 
        for (Person p : roster) {
               //get name 
               //get mailId
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}
        
 */
package com.kpr.training.java.exercise3.collections.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPerson {
    
    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>();
        
        for (Person person : roster) {
            name.add(person.getName());
            mailId.add(person.getEmailAddress());
        }
        
        String minimalName = Collections.min(name);
        String minimalMailId = Collections.min(mailId);

        System.out.println("The Minimal Person Name is " + minimalName + 
				   		   "\nThe Minimal EmailId is " + minimalMailId);
        
    }
}
