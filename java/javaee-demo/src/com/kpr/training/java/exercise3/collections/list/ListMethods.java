/* 
Requirement:
    To explain about contains(), subList(), retainAll() and give example.
    
Entity:
    ListMethods
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a new Generic List of Integer as list1 and list2.
    2) List has the methods of contains(), subList(), retainAll().
    3) contains() method is used to find whether the element present or not.
    4) subList() method is used to get particular part of index, it has fromIndex and toIndex.
    5) retainAll() method is used to find common elements between two list.
    
Pseudo code:
	public class ListMethods {
		
	    public static void main(String[] args) {
			
			List<Integer> list1 = new ArrayList<Integer>();
			
			// Add 10 elements to the list1
			
	    List<Integer> list2 = new ArrayList<Integer>();
			
			// Add 5 elements to the list2
			
			// Invoke contains() method to check the element present or not
			// print the result
			
			// Invoke subList() method to print the sublist of a list
			
			// Invoke the method retainAll() and print the result
	    }	
	}
 */

package com.kpr.training.java.exercise3.collections.list;

import java.util.ArrayList;
import java.util.List;

public class ListMethods {
	
    public static void main(String[] args) {
		
		List<Integer> list1 = new ArrayList<Integer>();
		
		list1.add(10);
		list1.add(20);
		list1.add(30);
		list1.add(40);
		list1.add(50);
		list1.add(60);
		list1.add(70);
		list1.add(80);
		list1.add(90);
		list1.add(100);
		
		List<Integer> list2 = new ArrayList<Integer>();
		
		list2.add(10);
		list2.add(20);
		list2.add(130);
		list2.add(140);
		list2.add(50);
		
		//contains()
		System.out.println(list1.contains(100));
		System.out.println(list1.contains(200));
		
		//subList()
		List<Integer> sublist1 = new ArrayList<Integer>(list1.subList(1, 6));
		System.out.print("\nSubList of a list : ");
		System.out.print(sublist1);
		
		//retainAll();
		list1.retainAll(list2);
		System.out.print("\nCommon values of two list using retainAll : ");
		System.out.print(list1);
    }	
}
