/* 
Requirement:
	Handle and give the reason for the exception in the following code:
 	PROGRAM:
 	public class Exception {  
     	public static void main(String[] args) {
          	int arr[] ={1,2,3,4,5};
          	System.out.println(arr[7]);
     	}
 	}
 	Display the output.
  
Entity:
    DisplayOutput
    
Function Declaration:
    public static void main(String[] args)
Jobs To Be Done:
     1) Create an array as type Integer and store in the variable arr.
     2) Print the 7th index value of the array
         2.1) Throw an exception ("Array index out of range")
         
Pseudo code:
	public class DisplayOutput {
		
		public static void main(String[] args) {
	 
		    int[] arr ={1,2,3,4,5};
		
		    try {
		    	
	            System.out.println(arr[7]);
		    } 
		    // Catch the exception ("Array index out of range")
	    }
	}
*/

package com.kpr.training.java.exercise3.exceptionhandling;

public class DisplayOutput {
	
	public static void main(String[] args) {
 
	    int arr[] ={1,2,3,4,5};
	
	    try {
	    	
            System.out.println(arr[7]);
	    } 
	    catch(ArrayIndexOutOfBoundsException e) {
	    	
		    System.out.println("Array index out of range");
	    }
    }
}
