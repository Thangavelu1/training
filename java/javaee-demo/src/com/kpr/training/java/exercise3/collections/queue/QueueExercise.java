/* 
Requirement:
    Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
		  -> add atleast 5 elements
		  -> remove the front element
		  -> search a element in stack using contains key word and print boolean value value
		  -> print the size of stack
		  -> print the elements using Stream 

Entity:
    QueueExercise
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a Generic Queue of String as queue and using LinkedList Implementation.
    2) Create a Generic Queue of String as queue and using PriorityQueue Implementation.
    3) Add 5 elements to both queue and queue2.
    4) Taking an element from the queue using poll() and remove() method.
    5) After taking the element print the queue and queue2.
    6) Invoke the method contains() to find whether the element present or not.
    7) Invoke the method size() to find the size of the queue.
    8) Stream is used to print the elements in both queue.
    
Pseudo code:
	public class QueueExercise {
		
		public static void main(String[] args) {
			
			Queue<String> queue = new LinkedList<String>();
			Queue<String> queue2 = new PriorityQueue<String>();
			
			// Add 5 elements to the queue
			
			// Add 5 elements to the queue2
			
			System.out.println("Before taking Element");
			// Print the elements in both Queue
			
			queue.poll();
			queue2.remove();
			
			System.out.println("After taking Element");
			// Print the elements in both Queue
			
			// Invoke the method contains() and print the results
			
			// Invoke the method size() and print the results
			
			System.out.println("Elements in queue :");
			// Print the elements using stream
			
			System.out.println("Elements in queue2 :");
			// Print the elements using stream
		}
	}
 */

package com.kpr.training.java.exercise3.collections.queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueExercise {
	
	public static void main(String[] args) {
		
		Queue<String> queue = new LinkedList<String>();
		Queue<String> queue2 = new PriorityQueue<String>();
		
		queue.add("1");
		queue.add("2");
		queue.add("3");
		queue.add("4");
		queue.add("5");
		
		queue2.offer("6");
		queue2.offer("7");
		queue2.offer("8");
		queue2.offer("9");
		queue2.offer("10");
		
		System.out.println("Before taking Element");
		System.out.println(queue);
		System.out.println(queue2);
		
		queue.poll();
		queue2.remove();
		
		System.out.println("After taking Element");
		System.out.println(queue);
		System.out.println(queue2);
		
		System.out.println(queue.contains("3"));
		System.out.println(queue2.contains("11"));
		
		System.out.println("Size of the queue is : " + queue.size());
		System.out.println("Size of the queue2 is : " + queue2.size());
		
		System.out.println("Elements in queue :");
		Stream<String> stream = queue.stream();
		stream.forEach(e -> {
			System.out.println(e);
		});
		
		System.out.println("Elements in queue2 :");
		Stream<String> stream2 = queue2.stream();
		stream2.forEach(e -> {
			System.out.println(e);
		});
	}
}
