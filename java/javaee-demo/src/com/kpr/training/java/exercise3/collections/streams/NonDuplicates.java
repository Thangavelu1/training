/*
Requirement:
      Consider a following code snippet:
  List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
     - Get the non-duplicate values from the above list using java.util.Stream API
     
Entity:
    NonDuplicates
    Person
    
Function Signature:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a list as type integer and store the list in the variable randomNumbers.
    2) Create the List as type integer to store non duplicate values in the variable nonDuplicate.
    3) Print the elements using stream.
   
Pseudo code:

	public class NonDuplicates {
	 
		public static void main(String[] args) {
	    	List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
			// Filter the elements which is non repeated and store it in.
			Stream<Integer> stream = withoutDuplicate.stream();
			stream.forEach(elements -> System.out.print(elements + " "));
	    }
	}
     
 */

package com.kpr.training.java.exercise3.collections.streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicates {
    
    public static void main(String[] args) {
    	
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        
        List<Integer> nonDuplicate = randomNumbers.stream()
                									  .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                									  .collect(Collectors.toList());
        
        Stream<Integer> stream = nonDuplicate.stream();
        stream.forEach(elements -> System.out.println(elements));
    }
}
