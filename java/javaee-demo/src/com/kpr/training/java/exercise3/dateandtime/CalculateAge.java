/*
Requirement:
    To write a java code to print my age
    
Entity:
    CalculateAge
    
Function declaration:
    public static void man(String[] args)
    
Jobs to be done:
    1) Create a reference for LocalDate as dateOfBirth and store the birth date.
    2) Create a reference for LocalDate as presentDate and invoke now() method to get current date, month and year and store.
    3) Create a reference for Period as myAge
    	3.1) Invoke the between() method and pass the dateOfBirth and presentDate and
    	3.2) Store it in the variable myAge.
    4) Print the age by invoking getYears() method.
    
Pseudo code:
public class CalculateAge {
    
    public static void main(String[] args) {
    	
        LocalDate dateOfBirth = LocalDate.of(2001, 1, 17);
        LocalDate presentDate = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, presentDate);
        
        System.out.println("I am " + myAge.getYears() + " years old");
    }
}
*/

package com.kpr.training.java.exercise3.dateandtime;

import java.time.LocalDate;
import java.time.Period;

public class CalculateAge {
    
    public static void main(String[] args) {
    	
        LocalDate dateOfBirth = LocalDate.of(2001, 1, 17);
        LocalDate presentDate = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, presentDate);
        
        System.out.println("I am " + myAge.getYears() + " years old");
    }
}
