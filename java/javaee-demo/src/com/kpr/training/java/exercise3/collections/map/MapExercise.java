/* 
Requirement:
    To Write a Java program to copy all of the mappings from the specified map to another map.
    
Entity:
    MapExercise
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create new map is created using TreeMap.
    2) Put the values to the map using put() method.
    3) subMap is created using SortedMap and TreeMap.
    4) Another TreeMap map2 is created.
    5) Using putAll() method map values inserted to map2.
    6) To print the map2.
    7) size() method is used to find the size of the map.
    8) subMap is used to get the required keys and values by giving fromKey and toKey.
    9) Print the keys and values of subMap.
   
Pseudo code:
	public class MapExercise {
		
		public static void main(String[] args) {
			
			TreeMap<Integer, String> map = new TreeMap<Integer, String>();
			SortedMap<Integer, String> subMap = new TreeMap<Integer, String>();
			
			// Put 5 elements to the map
			
			TreeMap<Integer, String> map2 = new TreeMap<Integer, String>();
			// Using putAll() method map values inserted to map2.
			// Print the map
			
			// Print the size of the map
			
			subMap = map.subMap(2,5);
			// print the subMap of map
			
		}
	}
 */

package com.kpr.training.java.exercise3.collections.map;

import java.util.SortedMap;
import java.util.TreeMap;

public class MapExercise {
	
	public static void main(String[] args) {
		
		TreeMap<Integer, String> map = new TreeMap<Integer, String>();
		SortedMap<Integer, String> subMap = new TreeMap<Integer, String>();
		
		map.put(1, "Aravind");
		map.put(2, "Abi");
		map.put(3, "Arjun");
		map.put(4, "Anwar");
		map.put(5, "Anbu");
		
		TreeMap<Integer, String> map2 = new TreeMap<Integer, String>();
		map2.putAll(map);
		System.out.println(map2);
		
		System.out.println("Size of the HashMap is : " + map.size());
		
		subMap = map.subMap(2,5);
		System.out.println("Submap of the map is :" + subMap);
		
	}
}
