/*
Requirement: 
	Get the file names of all file with specific extension in a directory.

Entity:
	FileName

Function Declaration :
	public static void main(String[] args) {}

Jobs To Be Done:
	1) Create a object for File with directory as argument.
	2) Add all file names to the array.
	3) for each file name in the array.
		3.1) check whether the extension matches with given extension.
			3.1.1) if matches, Print the file names.

PseudoCode:
	public class FileName {
	
		public static void main(String[] args) {
			
			File file = new File("C:\\1dev\\database\\training\\java\\javaee-demo\\"
					+ "src\\com\\kpr\\training\\java\\exercise3\\advanced");
			File[] fileArray = file.listFiles();
			
			for (File files : fileArray ) {
				
				if(files.getName().toLowerCase().endsWith(".txt")) {
					
					System.out.println(files.getName());
				}
			}	
		}
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.File;

public class FileName {

	public static void main(String[] args) {
		
		File file = new File("C:\\1dev\\database\\training\\java\\javaee-demo\\"
				+ "src\\com\\kpr\\training\\java\\exercise3\\advanced");
		File[] fileArray = file.listFiles();
		
		for (File files : fileArray ) {
			
			if(files.getName().toLowerCase().endsWith(".txt")) {
				
				System.out.println(files.getName());
			}
		}	
	}
}
