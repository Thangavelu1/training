/*
Requirement:
	Write a program to add 5 elements to a xml file and print the elements in the xml file using list. 
  	Remove the 3rd element from the xml file and print the xml file.
  
Entity:
   	PropertiesXML
   
Function Declaration:
   	public static void main(String[] args) {}
   
Jobs To Be Done:
    1) Create a object for properties.
    2) Setting some property as (key - value) pair in properties.
    3) Create a object for FileOutputStream and assign a name for xml file.
    4) Store the properties in FileOutputStream as xml file.
    5) Print the properties stored in xml file.
    6) Print the elements in xml file using list. 
    7) Remove the 3rd element and print the properties.
    
Pseudo code:
    class PropertiesXML {
    
        public static void main(String[] args) throws IOException {
            // Add the element to the properties.
            OutputStream output = new FileOutputStream("Filename.xml");
            // Store the property element to the Outputstream.
            System.out.println("Properties stored in xml file");
            
            properties.list(System.out);
    
            properties.remove("3rd element");
            System.out.println("After removing the element from the xml file ");
            properties.list(System.out);
        }
    }   
*/

package com.kpr.training.java.exercise3.collections.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesXML {

    public static void main(String[] args) throws IOException {
    	
        Properties properties = new Properties();
        
        properties.setProperty("Cricket", "Virat Kohli");
        properties.setProperty("Foot Ball", "Cristiano Ronaldo");
        properties.setProperty("Badminton", "P. V. Sindhu");
        properties.setProperty("Basketball", "LeBron James");

        OutputStream output = new FileOutputStream("SportsPlayers.xml");
        
        properties.storeToXML(output, "Sports and Players");
        
        System.out.println("Properties stored in xml file");

        properties.list(System.out);

        properties.remove("Sri Lanka");
        
        System.out.println("After removing the element from the xml file ");
        
        properties.list(System.out);
    }
}
