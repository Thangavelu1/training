/*
Requirement :
    Write a generic method to exchange the positions of two different elements in an array.

Entity:
    Exchange

Function Declaration:
    public static <S> void swap(S[] list, int index1, int index2) {}
    public static void main(String[] args) {}

Jobs to be done:
    1) Create a method swap() and swap the elements using temporary variables.
    2) Create a String Array as name and store the names.
    3) Print the array before Swapping.
    4) Invoke the method swap() and pass the parameter name, index1 and index2.
    5) Print the array after Swapping.
    
Pseudo code:
	public class Exchange {
		
	    public static <T> void swap(T[] list, int index1, int index2) {
	    	
	        // Swap the two string
	    }
	
	    public static void main(String[] args) {
	    	
	        String[] nameList = {"Abi", "Banu", "Cheng", "David", "Edward", "Finch"};
	        
	        System.out.print("Before Swapping : ");
	        System.out.print("\t" + Arrays.toString(nameList));
	        
	        swap(nameList, 2, 4);
	        
	        System.out.print("\nAfter Swapping : ");
	        System.out.print("\t" + Arrays.toString(nameList));
	    }
	}
*/

package com.kpr.training.java.exercise3.generics;

import java.util.Arrays;

public class Exchange {
	
    public static <T> void swap(T[] list, int index1, int index2) {
    	
        T temporary = list[index1];
        list[index1] = list[index2];
        list[index2] = temporary;
    }

    public static void main(String[] args) {
    	
        String[] name = {"Abi", "Banu", "Cheng", "David", "Edward", "Finch"};
        
        System.out.print("Before Swapping : ");
        System.out.print("\t" + Arrays.toString(name));
        
        swap(name, 2, 4);
        
        System.out.print("\nAfter  Swapping : ");
        System.out.print("\t" + Arrays.toString(name));
    }
}