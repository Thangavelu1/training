/*Requirement:
 * 		 To read the file contents  line by line in streams with example.
 * 
 * Entity: 
 * 		 ReadLineByLine
 * 
 * Method Signature: 
 * 		public static void main(String[] args)
 * 
 * Jobs to be done:
 * 		 1.The path of the file is stored in filePath
 *       2. Read the file content in lines as stream.
 * 		 3. For each line
 *           3.1. Print the line.
 * 					
 * PseudoCode:
 * 
public class ReadFileDemo {
	
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get("C:\\1dev\\training\\java\\javaee-demo\\apple.txt");
		Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);		
	}
}

 */

package com.kpr.training.java.exercise3.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadLineByLine {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		
		Path filePath = Paths.get("D:\\File\\sample.txt");
		Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);		
	}
}