/*
Requirement:
    To Find which one of these is a valid lambda expression? and why?:
             (int x, int y) -> x+y; or (x, y) -> x + y;
    To Find what is wrong with the following interface? and fix it.
        (int x, y) -> x + y;
        
Entity:
    ValidExpression
    
Function Declaration: 
    public static void main(String[] args) {}
    
Jobs To be Done:
    1) Checking the valid expression.
    
Solution:
    (int x, int y) -> x+y; or (x, y) -> x + y;
	In the given expression, both the expressions are valid expression.
    Because the major difference between two expression is identifier .
    In lambda expression we may or may not include datatype.
    
    (int x, y) -> x + y;
    In the given expression, The data type identifier must be given to both variables otherwise 
    both variables doesn't have data type identifier
*/

package com.kpr.training.java.exercise3.lambdaexpression;

interface Validator {
	
	public int valid(int x, int y);
}

public class ValidExpression {
	
	public static void main(String[] args) {
		
		Validator validExp = (int x, int y) -> x+y;
		System.out.println(validExp.valid(10, 20));
		
		Validator validExp1 = (x, y) -> x + y;
		System.out.println(validExp1.valid(10, 20));
		
		//Validator validExp2 = (int x, y) -> x + y;
		//System.out.println(validExp2.valid(10, 20));
	}
}
