/*
Requirement:
    add and remove the elements in stack
    
Entity:
    StackAddRemove
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1) Get the input from the user
    2) Create an empty stack as stack
    3) Add elements to the stack
    4) Remove elements using index in the stack
    
Pseudo code:
    public class StackAddRemove {
        public static void main(String[] args) {
            Stack<Integer> stack = new Stack<Integer>();
            //Add 5 elements to the stack
            //Add the input elements to the stack
            //Remove the elements using index in the stack
        }
    }
*/

package com.kpr.training.java.exercise3.collections.stack;

import java.util.Stack;

public class StackAddRemove {
	
	public static void main(String[] args) {
		
		Stack<Integer> stack = new Stack<Integer>();
		
		stack.add(10);
		stack.add(20);
		stack.add(30);
		stack.add(40);
		stack.add(50);
		
		System.out.println(stack);
		
		int removeElement = stack.remove(3);
		System.out.println("Removed element from the stack is : " + removeElement);
		System.out.println(stack);
	}
}
