/*
Requirement:
    create a pattern for password which contains
	   8 to 15 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit.
       
Entity:
    PasswordPattern
    
Function Declaration:
    public static boolean ValidName(String name)
    public static void main(String[] args) {}
     
Jobs to be Done:
    1) Get the input from the user and store it in the variable word.
    2) Invoke the method validPassword() and pass the word as parameter.
    3) Check if the password matches
    	3.1) Print "Valid Password"
    4) else print "Valid Password"
    5) Create a method validPassword()
     	5.1) Check whether the word is null.
     	5.2) Create a regex pattern for the given conditon and store it in the variable regex as type String
     	5.3) Compile the regex pattern Using Pattern
     	5.4) Match the pattern with input using Matcher
     	5.5) Return the boolean output
     
Pseudo code:
	public class PasswordPattern {
	
	    public static boolean validPassword(String word) {
	    	
	    	if (word == null) {
	            return false;
	        }
	    	
	        // Create a regex pattern for the given conditon
	
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(word);
	        
	        return matcher.matches();
	    }
	
	    public static void main(String[] args) {
	    	
	        Scanner scanner = new Scanner(System.in);
	        
	        System.out.println("Enter the name ");
	        String word = scanner.next();
	        
	        if (validPassword(word)) {
	        	
	        	System.out.print("Valid Password");
	        }
	        else {
	        	
	        	System.out.print("Invalid Password");
	        }
	        
	        scanner.close();
	    }
	}
 */
package com.kpr.training.java.exercise3.regularexpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordPattern {

    public static boolean validPassword(String word) {
    	
    	if (word == null) {
            return false;
        }
    	
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(word);
        
        return matcher.matches();
    }

    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter the name ");
        String word = scanner.next();
        
        if (validPassword(word)) {
        	
        	System.out.print("Valid Password");
        }
        else {
        	
        	System.out.print("Invalid Password");
        }
        
        scanner.close();
    }
}
