/*
Requirement:
    To check the given path is file or directory.
   
Entity:
    FileOrDirectory
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Done:
    1) Get the file or directory path.
    2) Check whether the given path is file or directory.
    3) if it is file 
        3.1) Print this is file

Pseudo code:
	public class FileOrDirectory {
	
		public static void main(String args[]) {
			
			File file = new File("C:\\1dev\\database\\training\\java\\javaee-demo");
			
			if (file.isFile()) {
				
				System.out.println("This is file");
			} else {
				
				System.out.println("This is directory");
			}
		}
}  
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.File;

public class FileOrDirectory {

	public static void main(String[] args) {
		
		File file = new File("C:\\1dev\\database\\training\\java\\javaee-demo");
		
		if (file.isFile()) {
			
			System.out.println("This is file");
		} else {
			
			System.out.println("This is directory");
		}
	}
}
