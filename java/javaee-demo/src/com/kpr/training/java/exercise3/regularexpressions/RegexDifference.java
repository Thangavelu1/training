/*
Requirement:
	demonstrate establish the difference between lookingAt() and matches().
	
Entity:
	RegexDifference
	
Function Declaration:
	public static void main(String[] args) {}
	
Jobs To Be Done:
	1) Get the input from the user and store it in the variable name.
	2) Create a regex pattern using pattern 
	3) Match the input using matcher
	4) Invoke the method lookingAt() and print the result.
	5) Invoke the method matches() and print the result.
	
Pseudo code:
	public class RegexDifference {
	    
	    public static void main( String args[] ) {
	    	
	    	String regex = "T";
	    	Scanner scanner = new Scanner(System.in);
	    	System.out.println("Enter the name");
	    	String name = scanner.nextLine();
	    	
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(name);
			
			System.out.println("REGEX is : "+ regex);
			System.out.println("INPUT is : "+ name);
			
			System.out.println("Using lookingAt(): "+matcher.lookingAt());
			System.out.println("Using matches(): "+matcher.matches());
			
			scanner.close();
	    }
	}
	 
 */

package com.kpr.training.java.exercise3.regularexpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDifference {
    
    public static void main( String args[] ) {
    	
    	String regex = "T";
    	Scanner scanner = new Scanner(System.in);
    	System.out.println("Enter the name");
    	String name = scanner.nextLine();
    	
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(name);
		
		System.out.println("REGEX is : "+ regex);
		System.out.println("INPUT is : "+ name);
		
		System.out.println("Using lookingAt(): "+matcher.lookingAt());
		System.out.println("Using matches(): "+matcher.matches());
		
		scanner.close();
    }
}
