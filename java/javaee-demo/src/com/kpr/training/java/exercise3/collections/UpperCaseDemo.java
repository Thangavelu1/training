/*
Requirement:
     8 districts are shown below
 Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 
Entity:
     UpperCaseDemo
      
Function Declaration:
     public static void main(String[] args) {}
     
Jobs to be done:
     1) Create class named as UpperCaseDemo.
     2) Create reference for list and assign all values given.
     3) Change all the string value in uppercase.
     4) Print the list.
     
Pseudo code:
    public class UpperCaseDemo {
   
        public static void main(String[] args) {
      
      		List<String> list = new LinkedList<>();
        	//add the elements 
        	//use toUpperCase method to be converted to UPPERCASE.
        	//print the list.
      	}
	}      
*/

package com.kpr.training.java.exercise3.collections;

import java.util.LinkedList;
import java.util.List;

public class UpperCaseDemo {
	
	public static void main(String[] args) {
    	
        List<String> list = new LinkedList<>();
        
        list.add("Madurai");
        list.add("Coimbatore");
        list.add("Theni");
        list.add("Chennai");
        list.add("Karur");
        list.add("Salem");
        list.add("Erode");
        list.add("Trichy");
        
        list.replaceAll(s -> s.toUpperCase());
        
        System.out.println(list);
    }
}
