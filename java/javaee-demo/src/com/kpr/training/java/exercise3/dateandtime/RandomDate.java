/*
Requirement: 
	To find the previous friday when given with a date.

Entity: 
	PreviousFriday

Function Declaration: 
	public static void main(String[] args) {}

Jobs to be done:
	1) Create a reference for LocalDate and store the random date.
	2) Find the Previous Friday from the random date date and print the Previous Friday.

PseudoCode:
public class RandomDate {
	
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2020, 10, 1);
		System.out.println("The previous Friday is " + date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
	}
}
*/
package com.kpr.training.java.exercise3.dateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class RandomDate {
	
	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2020, 10, 1);
		System.out.println("The previous Friday is " + date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
	}
}
