/*
Requirement:
    To use Multiple Catch block with example.
    
Entity:
    MultipleCatchBlock
    
Function declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object for Integer array with list of numbers
    2) Iterate the array using for loop
    	2.1) Print the result for 2/array[i]
    	    2.1.1) Catch the exception "ArithmeticException" if it is thrown
    	    2.1.2) Catch the exception if any other exception is thrown
   
Pseudo code:
	public class MultipleCatchBlock {
		
		public static void main(String[] args) {
			
	        int[] array = new int[] {1,2,3,0,5};
	        
			for(int i=0; i<6; i++) {
				
			    try {
			    	
				    // Print the result for 2/array[i]
				}
				
			    // Catch the ArithmeticException
			    
			    // Catch the Exceptions which is thrown
			} 
		}
	}
 */

package com.kpr.training.java.exercise3.exceptionhandling;

public class MultipleCatchBlock {
	
	public static void main(String[] args) {
		
        int[] array = new int[] {1,2,3,0,5};
        
		for(int i=0; i<6; i++) {
			
		    try {
		    	
			    System.out.println(2/array[i]);
			}
		    catch(ArithmeticException e) {
		    	
				System.out.println("divided by 0");
		    }
		    catch(Exception e) {
		    	
				System.out.println("Exception handled");
		    }
		} 
	}
}
