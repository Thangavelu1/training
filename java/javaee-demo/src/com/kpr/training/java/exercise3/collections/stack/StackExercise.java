/* 
Requirement:
    Create a stack using generic type and implement
		-> Push atleast 5 elements
		-> Pop the peek element
		-> search a element in stack and print index value
		-> print the size of stack
		-> print the elements using Stream
    Reverse List Using Stack with minimum 7 elements in list.
    
Entity:
    StackExercise
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a Generic Stack of String as stack.
    2) Add 5 values to the stack.
    3) Invoke pop() method to print the top element.
    4) Invoke search() method to search the index of the element.
    5) Invoke size() method to find the size of the stack.
    6) Stream used to iterate the element.
    7) Print the elements in the stack using stream.
    8) Create a generic list of String as list.
    9) Create another generic stack as stack2.
    10) Add 7 elements to the stack.
    11) To reverse the list two while conditions are used.
    12) First while has the condition of list.size()>0.
    13) First while is to remove first value from list and the value is pushed to stack2.
    14) Next while has the condition of stack.size()>0.
    15) Next while is used to add first element of stack2 to the list using pop() method. 

Pseudo code:
public class StackExercise {
	
	public static void main(String[] args) {
		
		Stack<String> stack = new Stack<String>();
		
		// Add 5 values to the stack.
		
		String topElement = stack.pop();
		System.out.println(topElement);
		
		int index = stack.search("3");
		System.out.println("Index of an Element :" + index);
		
		int size = stack.size();
		System.out.println("Size fo the satck:" + size);
		
		// Print the stack using stream
		
		List<String> list = new ArrayList<String>();
		
		// Add 7 elements to the stack.
		
		System.out.println(list);

		Stack<String> stack2 = new Stack<String>();
		
		while(list.size() > 0) {
		    stack2.push(list.remove(0));
		}

		while(stack2.size() > 0){
		    list.add(stack2.pop());
		}

		System.out.println(list);
	}
}
 */

package com.kpr.training.java.exercise3.collections.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Stream;

public class StackExercise {
	
	public static void main(String[] args) {
		
		Stack<String> stack = new Stack<String>();
		
		stack.push("1");
		stack.push("2");
		stack.push("3");
		stack.push("4");
		stack.push("5");
		
		String topElement = stack.pop();
		System.out.println(topElement);
		
		int index = stack.search("3");
		System.out.println("Index of an Element :" + index);
		
		int size = stack.size();
		System.out.println("Size fo the satck:" + size);
		
		Stream<String> stream = stack.stream();
		stream.forEach((element) -> {
			System.out.println(element);
		});
		
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");
		list.add("E");
		list.add("F");
		list.add("G");
		
		System.out.println(list);

		Stack<String> stack2 = new Stack<String>();
		
		while(list.size() > 0) {
		    stack2.push(list.remove(0));
		}

		while(stack2.size() > 0){
		    list.add(stack2.pop());
		}

		System.out.println(list);
	}
}
