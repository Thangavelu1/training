/*
Requirement:  
	Write an example that, for a given year, reports the length of each month within
              that particular year.

Entity: 
	LengthOfMonthParticularYear

Function Declaration: 
	public static void main(String[] args) {}

Jobs to be done:
		1) Assign the input year to year variable. 
		2) For each month in the year.
			2.1) Find the number of days 
			2.2) Print the month of the year and days of month.
PseudoCode:
public class LengthOfMonthParticularYear {
	
	public static void main(String[] args) {
        int year = 2020;
        
        System.out.println("For the year " + year);
        
        for (Month month : Month.values()) {
        	
            YearMonth yearMonth = YearMonth.of(year, month);
            System.out.println(month + ":" + yearMonth.lengthOfMonth() + "days");
        }
    }
}
*/
package com.kpr.training.java.exercise3.dateandtime;

import java.time.Month;
import java.time.YearMonth;

public class LengthOfMonthParticularYear {
	
	public static void main(String[] args) {
        int year = 2020;
        
        System.out.println("For the year " + year);
        
        for (Month month : Month.values()) {
        	
            YearMonth yearMonth = YearMonth.of(year, month);
            System.out.println(month + ":" + yearMonth.lengthOfMonth() + "days");
        }
    }
}
