/*
Requirement: 
      To find what's wrong with the following program? And fix it using Type Reference

			public interface BiFunction{
			    int print(int number1, int number2);
			}
			
			public class TypeInferenceExercise {
			    public static void main(String[] args) {
			
			        BiFunction function = (int number1, int number2) ->  { 
			        return number1 + number2;
			        };
			        
			        int print = function.print(int 23,int 32);
			        
			        System.out.println(print);
			    }
			}
Entity:
     TypeInference
     
Function Declaration :
     int print(int number1, int number2),
     public static void main(String[] args) {}
     
Jobs To Be Done:
     1) To find what is wrong in the above program.
 */

package com.kpr.training.java.exercise3.lambdaexpression;

interface BiFunction{ 
	
	// public is removed
    int print(int number1, int number2);
}

public class TypeInference {
	
	public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(111, 222);
        
        System.out.println(print);
    }
}
