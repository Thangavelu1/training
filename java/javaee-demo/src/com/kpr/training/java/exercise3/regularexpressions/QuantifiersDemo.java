/*
Requirement:
	To write a program for java regex quantifier
	
Entity:
	QuantifiersDemo
	
Function Declaration:
	public class QuantifiersDemo {}
	
Jobs To Be Done:
	1) Declare the variable text and store the string value.
	2) Store the regular expressions to the variable regex, regex1, regex2, regex3, regex4, regex5.
	3) Compile the regex to the pattern.
	4) Match the pattern with the given string.
	5) Print the result for each regex.
	
Pseudo code:
	public class QuantifiersDemo {
	
		public static void main(String[] args) {
			
			String text = "abaabaaabaaaabaaaaaabaaaaaaaaaaa";
		
			// store the regular expression values to the respective variables
			// Using Pattern and Matcher find the matches
			// Print the result
	}
}
 */

package com.kpr.training.java.exercise3.regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuantifiersDemo {
	
	public static void main(String[] args) {
		
		String text = "abaabaaabaaaabaaaaaabaaaaaaaaaaa";
		
		String regex = "a*";
		String regex1 = "a+";
		String regex2 = "a?";
		String regex3 = "a{2}";
		String regex4 = "a{2,}";
		String regex5 = "a{2,6}";
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		System.out.println("First Quantifier using *");
		while (matcher.find()) {
			System.out.println("Pattern found from " + matcher.start() + 
	                " to " + (matcher.end()));
		}
		
		Pattern pattern1 = Pattern.compile(regex1);
		Matcher matcher1 = pattern1.matcher(text);
		System.out.println("Second Quantifier using +");
		while (matcher1.find()) {
			System.out.println("Pattern found from " + matcher1.start() + 
	                " to " + (matcher1.end()));
		}
		
		Pattern pattern2 = Pattern.compile(regex2);
		Matcher matcher2 = pattern2.matcher(text);
		System.out.println("Third Quantifier using ?");
		while (matcher2.find()) {
			System.out.println("Pattern found from " + matcher2.start() + 
	                " to " + (matcher2.end()));
		}
		
		Pattern pattern3 = Pattern.compile(regex3);
		Matcher matcher3 = pattern3.matcher(text);
		System.out.println("Fourth Quantifier using {2}");
		while (matcher3.find()) {
			System.out.println("Pattern found from " + matcher3.start() + 
	                " to " + (matcher3.end()));
		}
		
		Pattern pattern4 = Pattern.compile(regex4);
		Matcher matcher4 = pattern4.matcher(text);
		System.out.println("Fifth Quantifier using {2,}");
		while (matcher4.find()) {
			System.out.println("Pattern found from " + matcher4.start() + 
	                " to " + (matcher4.end()));
		}
		
		Pattern pattern5 = Pattern.compile(regex5);
		Matcher matcher5 = pattern5.matcher(text);
		
		System.out.println("Sixth Quantifier using {2,6}");
		while (matcher5.find()) {
			System.out.println("Pattern found from " + matcher5.start() + 
	                " to " + (matcher5.end()));
		}
	}
}
