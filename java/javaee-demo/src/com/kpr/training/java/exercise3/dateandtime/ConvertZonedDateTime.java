/* 
Requirement:
	Write a Java program to convert ZonedDateTime to Date.

Entity:
 	ConvertZonedDateTime
 
Function Declaration:
 	public static void main(String[] args) {}
 
Jobs To Be Done:
  	1)Create a reference for ZonedDateTime as zonedDateTime.
   	2)Create a reference for Instant class and convert the instant date from zonedDateTime.
   	3)Create a reference for Date class and convert the date from instant date.
   	4)Print the date. 

Pseudo code:
public class ConvertZonedDateTime {

	public static void main(String[] args) {
		
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
		Instant instant = zonedDateTime.toInstant();
		Date date = Date.from(instant);
		System.out.println(date);
	}
}
 */

package com.kpr.training.java.exercise3.dateandtime;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

public class ConvertZonedDateTime {

	public static void main(String[] args) {
		
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
		Instant instant = zonedDateTime.toInstant();
		Date date = Date.from(instant);
		System.out.println(date);
	}
}
