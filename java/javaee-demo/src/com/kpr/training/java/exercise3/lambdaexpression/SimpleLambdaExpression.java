/*
Requirement:
	Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }
        
Entity:
	SimpleExpression
	SimpleLambdaExpression
	
Function Declaration:
	public int getValue()
	public static void main(String[] args) {}
	
Jobs To Be Done:
	1) Create an Interface SimpleExpression and declare a method.
	2) Create lambda expression and return the value 5.
	3) print the value by invoking getValue() method.
	
Pseudo Code:
	interface SimpleExpression {
	
		//Declare the method getValue();
	}

	public class SimpleLambdaExpression {
	
		public static void main(String[] args) {
		
			//Create Lambda Expression and return value
			System.out.println(simple.getValue());
		}
	}
*/

package com.kpr.training.java.exercise3.lambdaexpression;

interface SimpleExpression {
	
	public int getValue();
}

public class SimpleLambdaExpression {
	
	public static void main(String[] args) {
		
		SimpleExpression simple = (() -> {
			return 5;
		});
					
		System.out.println(simple.getValue());
	}
}
