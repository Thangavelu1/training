/*
Requirement:
    To write a Java program to get the dates 10 days before and after today.
      1. Using Local date
      2. Using calendar
      
Entity:
    GetDates
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1) Create a reference for LocalDate as currentDay
    2) Invoke now() method to get the current date and store it in currentDay.
    3) Print the currentDay
    	3.1) Print before 10 days of current day.
    	3.2) Print after 10 days of current day.
    4) Create a reference for Calendar as calendar
    2) Invoke getInstance() method to get the current date and store it in calendar.
    3) Invoke getTime() method and Print the calendar
    	3.1) Invoke add() method to get before and after 10 days of current day.
    	3.2) Invoke getTime() method and Print before 10 days of current day.
    	3.3) Invoke getTime() method and Print after 10 days of current day.
    
Pseudo code:
public class GetDates {
    
    public static void main(String[] args) {
    	
        LocalDate currentDay = LocalDate.now();
        
        System.out.println("The current date is\t: " + currentDay);
        System.out.println("The day 10 days before the current day is\t: " + currentDay.plusDays(-10));
        System.out.println("The day 10 days after the current day is\t: "  + currentDay.plusDays(10));
        
        System.out.println();
        
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is\t: " + calendar.getTime());
        
        calendar.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is\t: " + calendar.getTime());
        
        calendar.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is\t: " + calendar.getTime());
    }
}
*/
package com.kpr.training.java.exercise3.dateandtime;

import java.time.LocalDate;
import java.util.Calendar;

public class GetDates {
    
    public static void main(String[] args) {
    	
        LocalDate currentDay = LocalDate.now();
        
        System.out.println("The current date is\t: " + currentDay);
        System.out.println("The day 10 days before the current day is\t: " + currentDay.plusDays(-10));
        System.out.println("The day 10 days after the current day is\t: "  + currentDay.plusDays(10));
        
        System.out.println();
        
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is\t: " + calendar.getTime());
        
        calendar.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is\t: " + calendar.getTime());
        
        calendar.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is\t: " + calendar.getTime());
    }
}