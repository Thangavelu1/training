/*
Requirement:
    Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression and functional interface.
    
Entity:
    LambdaFuntionalInterface
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1) Get the two input from the user
    2) Create an interface ArithmeticOperation and create a method for input
    3) Add the two input using lambda expression
    4) Subtract the two input using lambda expression
    5) Multiply the two input using lambda expression
    6) Divide the two input using lambda expression
        6.1) throw an exception if any exception occurs
    
Pseudo code:
	interface ArithmeticOperation {
		
		int input(int a,int b);
	}
	
	public class LambdaFunctionalInterface {
		
		public static void main(String[] args) {
			
			Scanner scanner = new Scanner(System.in);
			
			System.out.println("Enter the numbers : ");
			
			int firstNumber = scanner.nextInt();
			int secondNumber = scanner.nextInt();
			System.out.println("First Number : " + firstNumber);
			System.out.println("Second Number : " + secondNumber);
			
			ArithmeticOperation addition = (int a, int b) -> (a + b);
	        System.out.println("Addition of two number = " + addition.input(firstNumber, secondNumber));
	
	        ArithmeticOperation subtraction = (int a, int b) -> (a - b);
	        System.out.println("Subtraction of two number = " + subtraction.input(firstNumber, secondNumber));
	
	        ArithmeticOperation multiplication = (int a, int b) -> (a * b);
	        System.out.println("Multiplication of two number = " + multiplication.input(firstNumber, secondNumber));
	        
	        try {
	        	
	        	ArithmeticOperation division = (int a, int b) -> (a / b);
	            System.out.println("Division of two number = " + division.input(firstNumber, secondNumber));
	        	
	        } catch (Exception e) {
	        	System.out.println("Zero Division Error");
	        }
			scanner.close();
		}
	}
 */

package com.kpr.training.java.exercise3.collections;

import java.util.Scanner;

interface ArithmeticOperation {
	
	int input(int a,int b);
}

public class LambdaFunctionalInterface {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the numbers : ");
		
		int firstNumber = scanner.nextInt();
		int secondNumber = scanner.nextInt();
		System.out.println("First Number : " + firstNumber);
		System.out.println("Second Number : " + secondNumber);
		
		ArithmeticOperation addition = (int a, int b) -> (a + b);
        System.out.println("Addition of two number = " + addition.input(firstNumber, secondNumber));

        ArithmeticOperation subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction of two number = " + subtraction.input(firstNumber, secondNumber));

        ArithmeticOperation multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication of two number = " + multiplication.input(firstNumber, secondNumber));
        
        try {
        	
        	ArithmeticOperation division = (int a, int b) -> (a / b);
            System.out.println("Division of two number = " + division.input(firstNumber, secondNumber));
        	
        } catch (Exception e) {
        	System.out.println("Zero Division Error");
        }
		scanner.close();
	}
}
