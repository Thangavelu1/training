/* 
Requirement:
    Consider a following code snippet
     Queue bike = new PriorityQueue();    
     bike.poll();
     System.out.println(bike.peek());    
what will be output and complete the code.

Entity:
    BikeCode
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Created an object for queue as bike and implement PriorityQueue as type String
    2) Use poll() method and remove the first element in the queue.
    3) Use peek() method and get the first element of the queue.
    4) To print the first element (peek value) of the queue.
    
    //Output of the code is null.
    
Pseudo code:
	public class BikeCode {
		
		public static void main(String[] args) {
			
			Queue<String> bike = new PriorityQueue<String>();
			
			// Print the bike
			
	        // remove the first element in the queue.
	        
	        // print the first element of the queue.
			
		}
	}
 */

package com.kpr.training.java.exercise3.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class BikeCode {
	
	public static void main(String[] args) {
		
		Queue<String> bike = new PriorityQueue<String>();
		
		System.out.println(bike);
		
        bike.poll();
        
        System.out.println(bike.peek());
		
	}
}
