/* 
Requirement:
    To find the maximum element in the range [begin, end) of a list.


Entity:
    MaximalElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1) Create a method public T max(List<? extends T> list, int begin, int end).
    2) Compare the elements and find the maximum between the range and Store it in maximumElement and return it.
    3) Create an ArrayList type Integer as list.
    4) Add 10 elements to the list.
    5) Invoke the method max() and get the maximum element in the list.
    6) Print the result.
    
pseudo code:

	public class  MaximumElement {
    
    	public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        	T maximumElement = list.get(begin);
        	for (++begin; begin < end; ++begin) {
            	if (maximumElement.compareTo(list.get(begin)) < 0) {
                	maximumElement = list.get(begin);
            	}
        	}   
        	return maximumElement;
    	}
    
    	public static void main(String[] args) {
    	
        	List<Integer> list = new LinkedList<>();
        	//add the elements to the list
        	System.out.println(max(list, 0, list.size()));
    	}
	}
*/

package com.kpr.training.java.exercise3.generics;

import java.util.ArrayList;
import java.util.List;

public class  MaximumElement {
    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
    	
        T maximumElement = list.get(begin);
        
        for (++begin; begin < end; ++begin) {
        	
            if (maximumElement.compareTo(list.get(begin)) < 0 ) {
            	
                maximumElement = list.get(begin);
            }
        }
        return maximumElement;
    }
    
    public static void main(String[] args) {
    	
        List<Integer> list = new ArrayList<>();
        
        list.add(100);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(500);
        list.add(60);
        list.add(70);
        list.add(80);
        list.add(90);
        list.add(10);
        
        System.out.println(max(list, 0, list.size()));
    }
}