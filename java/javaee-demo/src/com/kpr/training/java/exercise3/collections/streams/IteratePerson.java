/*
Requirement:
    Iterate the roster list in Persons class and and print the person without using forLoop/Stream
      
Entity:
    IteratePerson
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1.Import List and Iterator.
    2.Create class Iterate.
    3.Create reference for list named as roster and assign method createRoster for it.
    4.Create reference for Iterator named as iterator and call the method iterator().
    5.Print each Person using Iterator method.
    
Pseudo code:
	public class IteratePerson {
	
	    public static void main(String[] args) {
	    	
	        List<Person> roster = Person.createRoster();
	        Iterator<Person> iterator = roster.iterator();
	        
	        while (iterator.hasNext()) {
	            Person person = iterator.next();
	            System.out.println(person.name + "\t" +person.birthday + "\t" +person.gender + "\t" + person.emailAddress);
	        }
	    }
	}
*/

package com.kpr.training.java.exercise3.collections.streams;

import java.util.Iterator;
import java.util.List;

public class IteratePerson {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        Iterator<Person> iterator = roster.iterator();
        
        while (iterator.hasNext()) {
        	
            Person person = iterator.next();
            System.out.println(person.name + "\t" +person.birthday + "\t" +person.gender + "\t" + person.emailAddress);
        }
    }
}
