/*
Requirement:
  - Private Constructor with main class.
  
Entity:
    PrivateConstructors
  
Function Declaration:
    public static void main(String[] args) {}
  
Jobs to be done:
    1) Invoke the method instanceMethod of class Test
    2) Create a constructor 
          2.1) Print as "private constructor".
          
Pseudo Code:
class Test { 
    private Test() {
        System.out.println("This is a private constructor.");
    }
    public static void instanceMethod() { 
        Test obj = new Test();
    }
}
class PrivateConstructor {
    public static void main(String[] args) {
       // call the instanceMethod()
       Test.instanceMethod();
    }   
}
*/

package com.kpr.training.java.exercise3.reflections;

class Test {
 
    private Test() {
    	
        System.out.println("private constructor.");
    }

    @SuppressWarnings("unused")
	public static void instanceMethod() {
    	
        Test test = new Test();
    }
}

public class PrivateConstructors {

    public static void main(String[] args) {
    	
        Test.instanceMethod();
    }
}
