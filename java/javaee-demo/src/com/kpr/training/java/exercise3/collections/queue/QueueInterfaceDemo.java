/*
Requirement:
    what is the difference between poll() and remove() method of queue interface?
    
Entity:
    QueueInterFaceDemo
    
Function Declaration:
    public static void main(String[] args) {} 
    
Jobs To Be Done:
    1.Create an empty Queue as queue.
    2.poll() method returns null.
    3.remove() method throws exception.
    
Pseudo code:
    public class QueueInterfaceDemo {
        public static void main(String[] args) {
            try {
                Queue<String> queue = new Queue<String>();
                String s = queue.poll();
                String s1 = queue.remove();
            } 
            // Catch the exception if thrown
        }
    }
*/

package com.kpr.training.java.exercise3.collections.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueInterfaceDemo {
	
	public static void main(String[] args) {
		
		try {
			Queue<String> queue = new LinkedList<>();
			
			String s = queue.poll();
			System.out.println(s);
			
			String s1 = queue.remove();
			System.out.println(s1);
		} catch (Exception e) {
			System.out.println("NoSuchElementException");
		}
	}	
}
