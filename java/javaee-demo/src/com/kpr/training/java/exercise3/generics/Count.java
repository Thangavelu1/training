/*
Requirement: 
     Write a generic method to count the number of elements in a collection that have a specific
		property (for example, odd integers, prime numbers, palindromes).

Entity:
     Count
     
Function Declaration :
     public static void main(String[] args) {}
     public static int count(ArrayList<Integer> list) {}
     
Jobs To Be Done:
     1) Create the count method which returns the count of odd numbers present in a list.
     2) create an object for ArrayList as list
     3) Add elements to the list.
     4) Invoke the count method and print the number of odd numbers.
     
Pseudo Code:
	public class Count {

 		public static int count(ArrayList<Integer> list) {

     		int c = 0;
     		for(int elements : list) {
     		
         		if(elements % 2 != 0) {
         	
             	c++;
         		}
     		}
     		return c;
 		}

 		public static void main(String[] args) {
 	
     		ArrayList<Integer> list = new ArrayList<>();
     		// Add 5 elements to the ArrayList
     		System.out.println("Number of odd integers : " + count(list));
 		}
	}
 */

package com.kpr.training.java.exercise3.generics;

import java.util.ArrayList;

public class Count {
    
    public static int count(ArrayList<Integer> list) {
    	
        int c = 0;
        for(int elements : list) {
        	
            if(elements % 2 != 0) {
            	
                c++;
            }
        }
        return c;
    }

    public static void main(String[] args) {
    	
        ArrayList<Integer> list = new ArrayList<>();
        
        list.add(1);
        list.add(3);
        list.add(5);
        list.add(7);
        list.add(10);
        
        System.out.println("Number of odd integers : " + count(list));
    }
}