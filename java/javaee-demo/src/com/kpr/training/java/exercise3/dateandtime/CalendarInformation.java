/*
Requirement:
    To write a Java program to get and display information (year, month, day, hour, minute) of a 
    default calendar.
    
Entity:
    CalendarInformation
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1) Create a reference for Calendar as calendar.
    2) Invoke the method getInstance() method and store it in calendar.
    3) Print year, month, day, hour, minute.
    
Pseudo code:
public class CalendarInformation {
    
 
    public static void main(String[] args) {
    	
        Calendar calendar = Calendar.getInstance();
        
        System.out.println("Year\t: " + calendar.get(Calendar.YEAR));
        System.out.println("Month\t: " + calendar.get(Calendar.MONTH));
        System.out.println("Date\t: " + calendar.get(Calendar.DATE));
        System.out.println("Hour\t: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute\t: " + calendar.get(Calendar.MINUTE));
    }
}
*/

package com.kpr.training.java.exercise3.dateandtime;

import java.util.Calendar;


public class CalendarInformation {
    
 
    public static void main(String[] args) {
    	
        Calendar calendar = Calendar.getInstance();
        
        System.out.println("Year\t: " + calendar.get(Calendar.YEAR));
        System.out.println("Month\t: " + calendar.get(Calendar.MONTH));
        System.out.println("Date\t: " + calendar.get(Calendar.DATE));
        System.out.println("Hour\t: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute\t: " + calendar.get(Calendar.MINUTE));
    }
}
