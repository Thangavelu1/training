/*
Requirement:
  To print the sum of the variable arguments.

Entity:
    VarargDemo
    VarArgsInterface

Function Declaration:
    public int add(int ... numbers)
    public static void main(String[] args) {}
    
Jobs To Be Done:
     1) Create a interface Vararg
     2) Declare a method public int add(int ... numbers)
     3) Under a main method Create a lambda expression which returns the sum of varargs.
     4) print the sum value.

pseudo code:
	interface VarArgsInterface {
	    
	    public int add(int ... numbers);
	}
	public class VarargDemo {
	    
	    public static void main(String[] args) {
	       
	    	VarargInterface value = (numbers) -> {
	           //return sum 
	        };
	        System.out.println("sum : " + value.add(10,10,10,10));
	    }
	}
*/

package com.kpr.training.java.exercise3.lambdaexpression;

interface Vararg {
    
    public int add(int ... numbers);
}
public class VarArgsInterface {
    
    public static void main(String[] args) {
       
    	Vararg value = (numbers) -> {
            int sum = 0;
            for(int number : numbers) {
                sum = sum + number;
            }
            return sum;
        };
        System.out.println("sum : " + value.add(10,10,10,10));
    }
}