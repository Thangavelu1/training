/* 
Requirement:
    only try block without catch and finally blocks.
    
Entity:
    TryWithoutCatch
    
Function declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1) Print the result for 2/0 in try block.
    2) No catch block.

*/

package com.kpr.training.java.exercise3.exceptionhandling;

public class TryWithoutCatchAndFinally {
	
    public static void main(String[] args) {
		/*
		try{
		   System.out.println(2/0);
		}
		*/
		//Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
		//Syntax error, insert "Finally" to complete BlockStatements
	}
}
