/*
Requirement:
    Create an array list with 7 elements, and create an empty linked list add all elements of the 
    array list to linked list ,traverse the elements and display the result.
    
Entity:
    LinkedListDemo
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object for ArrayList as arrayList and type Integer.
    2) Add the values in arrayList.
    3) Prints the arrayList elements.
    4) create an object for linkedList as linkedList and type Integer.
    5) Add all elements of the arrayList to linkedList.
    6) Using forEach loop, prints the linkedList elements. 
    
Pseudo code:
  
  public class LinkedListDemo {
   
      public static void main(String[] args) {
      
      	  ArrayList<Integer> arrayList = new ArrayList<>();
          //add elements to the ArrayList
          
          LinkedList<Integer> linkedList = new LinkedList<>();
          linkedList.addAll(arrayList);
            
          //print the values using for each loop
      }
 }
*/

package com.kpr.training.java.exercise3.collections.list;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListDemo {

    public static void main(String[] args) {
    	
        ArrayList<Integer> arrayList = new ArrayList<>();
        
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
       
        System.out.println("Array list elements are " + arrayList);
        
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(arrayList);
        
        System.out.print("Linkedlist elements are using forEach : ");
        
        for (int elements : linkedList) {
            System.out.print(elements + " ");
        }
    }
}