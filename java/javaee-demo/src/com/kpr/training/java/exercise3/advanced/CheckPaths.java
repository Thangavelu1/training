/*
Requirement:
    To create two paths and test whether they represent same path.
   
Entity:
    CheckPaths
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Done:
    1) Get the two different paths as path1 and path2.
    2) Check whether the paths represents the same path.

Pseudo code:
	public class CheckPaths {
	
		public static void main(String[] args) {
			
			Path path1 = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
					+ "src\\com\\kpr\\training\\java\\exercise3\\nio\\file");
			Path path2 = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
					+ "src\\com\\kpr\\training\\java\\exercise3\\nio\\file");
			System.out.println(path1.equals(path2));
		}
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.nio.file.Path;
import java.nio.file.Paths;

public class CheckPaths {

	public static void main(String[] args) {
		
		Path path1 = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
				+ "src\\com\\kpr\\training\\java\\exercise3\\nio\\file");
		Path path2 = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
				+ "src\\com\\kpr\\training\\java\\exercise3\\nio\\file");
		System.out.println(path1.equals(path2));
	}
}