/*
Requirement:
    Display the default time zone and Any three particular time zone as mentioned below.
     Sample output: 
        Displaying current of the particular TimeZones
        GMT-27-09-2020 Sunday 08:36:28 AM
        Europe/Copenhagen-27-09-2020 Sunday 10:36:28 AM
        Australia/Perth-27-09-2020 Sunday 04:36:28 PM
        America/Los_Angeles-27-09-2020 Sunday 01:36:28 AM
     
Entity:
    DisplayTimeZone
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1) Invoke seDefault() method and Set the default time zone to GMT using TimeZone.
    2) Create an instance for SimpleDateFormat as daeFormatand store the default date format.
    3) Create an instance for Date as date.
    4) Format the date as dateFormat and store it in the String variable currentDateTime.
    5) Print the currentDateTime for GMT.
    6) Create an instance for TimeZone as timeZone and store the Europe/Copenhagen timeZone
    	6.1) Invoke setTimeZone() method to set the Europe/Copenhagen timeZone.
    	6.2) Print the currentDateTime.
    7) Create an instance for TimeZone as timeZone1 and store the EAustralia/Perth timeZone
    	7.1) Invoke setTimeZone() method to set the Australia/Perth timeZone.
    	7.2) Print the currentDateTime.
    8) Create an instance for TimeZone as timeZone2 and store the America/Los_Angeles timeZone
    	8.1) Invoke setTimeZone() method to set the America/Los_Angeles timeZone.
    	8.2) Print the currentDateTime.
    
Pseudo code:
public class DisplayTimeZone {
    
    public static void main(String[] args) {
    	
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
        
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);
        System.out.println("GMT\t: " + currentDateTime);
        
        TimeZone timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Europe/Copenhagen \t: " + currentDateTime);
        
        TimeZone timeZone1 = TimeZone.getTimeZone("Australia/Perth");
        dateFormat.setTimeZone(timeZone1);
        currentDateTime = dateFormat.format(date);
        System.out.println("Australia/Perth \t: " + currentDateTime);
        
        TimeZone timeZone2 = TimeZone.getTimeZone("America/Los_Angeles");
        dateFormat.setTimeZone(timeZone2);
        currentDateTime = dateFormat.format(date);
        System.out.println("America/Los_Angeles \t: " + currentDateTime);
    }
}
 */

package com.kpr.training.java.exercise3.dateandtime;

import java.util.Date;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class DisplayTimeZone {
    
    public static void main(String[] args) {
    	
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
        
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);
        System.out.println("GMT\t: " + currentDateTime);
        
        TimeZone timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Europe/Copenhagen \t: " + currentDateTime);
        
        TimeZone timeZone1 = TimeZone.getTimeZone("Australia/Perth");
        dateFormat.setTimeZone(timeZone1);
        currentDateTime = dateFormat.format(date);
        System.out.println("Australia/Perth \t: " + currentDateTime);
        
        TimeZone timeZone2 = TimeZone.getTimeZone("America/Los_Angeles");
        dateFormat.setTimeZone(timeZone2);
        currentDateTime = dateFormat.format(date);
        System.out.println("America/Los_Angeles \t: " + currentDateTime);
    }
}