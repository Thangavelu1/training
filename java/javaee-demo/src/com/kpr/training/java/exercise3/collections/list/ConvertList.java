/* 
Requirement:
       Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
    
Entity:
    ConvertList
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create Generic list of list1 and list2 type of Integer.
    2) Add 10 elements to the list1 using add() method.
    3) Add 5 elements to the list2 using add() method.
    4) Using addAll() method to add list2 values to list1.
    5) Using indexOf() and lastIndexOf() methods to print the specific values in the list.
    6) For loop, For each loop, Iterator, Stream is used to print the elements in the list.
    7) Converting the list to Set and Array.
    8) Print the Set and Array using for each loop.

Pseudo code:
public class ConvertList {
	
	public static void main(String[] args) {
		
		List<Integer> list1 = new ArrayList<Integer>();
		
		// Add 10 elements to the list1 using add() method.
		
		List<Integer> list2 = new ArrayList<Integer>();
		
		// Add 5 elements to the list2 using add() method.
		
		list1.addAll(list2);
		
		// Print the list
		
		System.out.println(list1.indexOf(50));
		System.out.println(list1.lastIndexOf(100));
		
		// Using for loop to print the elements 
		
		// Using for each loop to print the elements
		
		// Using iterator to print the elements
		
		// Using stream to print the elements
		
		// convert list to set
		
		//convert list to array
	}
}
 */

package com.kpr.training.java.exercise3.collections.list;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;
import java.util.stream.Stream;

public class ConvertList {
	
	public static void main(String[] args) {
		
		List<Integer> list1 = new ArrayList<Integer>();
		
		list1.add(10);
		list1.add(20);
		list1.add(30);
		list1.add(40);
		list1.add(50);
		list1.add(60);
		list1.add(70);
		list1.add(80);
		list1.add(90);
		list1.add(100);
		
		List<Integer> list2 = new ArrayList<Integer>();
		
		list2.add(110);
		list2.add(120);
		list2.add(130);
		list2.add(140);
		list2.add(150);
		
		list1.addAll(list2);
		
		System.out.println(list1);
		
		System.out.println(list1.indexOf(50));
		System.out.println(list1.lastIndexOf(100));
		
		//for loop
		System.out.println("for loop");
		for(int i=0; i<list1.size(); i++) {
			
			System.out.print(list1.get(i)+" ");
		}
		
		//for each loop
		System.out.println("\nfor each loop");
		for(Integer l : list1) {
			
			System.out.print(l+" ");
		}
		
		//List Iterator
		ListIterator<Integer> lIterator = list2.listIterator();
		
		System.out.println("\nList iterator");
		while(lIterator.hasNext()) {
			
			System.out.print(lIterator.next()+" ");
			
		}
		
		//Stream api
		
		Stream<Integer> stream = list2.stream();
		System.out.println("\nStream api");
		stream.forEach(l -> System.out.print(l+" "));
		
		//convert list to set
		Set<Integer> hashSet = new HashSet<Integer>(list1);
		
		System.out.println("\nconvert list to set");
		for (Integer i : hashSet)
			System.out.print(i+" ");
		
		//convert list to array
		Integer[] list1Array = new Integer[list1.size()];
		list1Array = list1.toArray(list1Array);
		
		System.out.println("\nconvert list to array");
		for (Integer i : list1Array)
			System.out.print(i+" ");
	}
}
