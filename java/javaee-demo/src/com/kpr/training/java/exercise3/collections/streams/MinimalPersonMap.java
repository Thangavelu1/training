/*
Requirement:
     To print minimal person with name and email address from the Person class 
     	using java.util.Stream<T>#map API by referring Person.java

Entity:
     MinimalPersonMap
     Person

Function Declaration:
     public static void main(String[] args) {}

Jobs To Be Done:
     1) Create an object as roster for the method createRoster().
     2) Create a ArrayList of type String.
         2.1) Get the Person name using stream mapping
     3) Create a ArrayList of type String.
     	 3.1) Get the person name using stream mapping 
         3.1) Get the mailId using stream mapping.
     4) Create minimalName of type String and get the minimalName in name.
     5) Create minimalMailId of type String and get the minimalMailId in mailId
     6) Print the minimalName and minimalMailId.
     
Pseudo code:
	class MinimalPersonMap {
	
		public static void main(String[] args) {
		
	     	List<Person> roster = Person.createRoster();
	     	
		    //Get the person name using stream mapping 
		    //Get the mailId using stream mapping
		   
		    String minimalName = Collections.min(name);
		    String minimalId = Collections.min(mailId);

		    System.out.println("The Minimal Person Name is " + minimalName + 
        				   "\nThe Minimal EmailId is " + minimalMailId);
	 	}
	}
 */
package com.kpr.training.java.exercise3.collections.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MinimalPersonMap {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        
        ArrayList<String> name = (ArrayList<String>) roster.stream()
        												   .map(person -> person.getName())
        												   .collect(Collectors.toList());
        
        ArrayList<String> mailId = (ArrayList<String>) roster.stream()
        													 .map(person -> person.getEmailAddress())
        													 .collect(Collectors.toList());
        
        String minimalName = Collections.min(name);
        String minimalMailId = Collections.min(mailId);

        System.out.println("The Minimal Person Name is " + minimalName + 
        				   "\nThe Minimal EmailId is " + minimalMailId);
    }
}