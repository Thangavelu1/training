/*
Requirement:
    To convert the InputStream to String and vice versa.
   
Entity:
    StringInputStreamConversion
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Done:
    1) Create a reference for FileInputStream with file as constructor argument.
    2) Get the content of the file and convert to string
    3) Print the string
    3) Convert the given string into input stream 
    4) Write the converted content to a file
    5) Close the created output stream.

Pseudo code:
	public class StringInputStreamConversion {
		
	    public static void main(String args[]) throws IOException {
	 	   
	        InputStream inputStream = new FileInputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
	        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
	        
	        Scanner scanner = new Scanner(inputStream);
	        StringBuffer stringBuffer = new StringBuffer();
	        
	        while(scanner.hasNext()) {
	     	   
	            stringBuffer.append(scanner.nextLine());
	        }
	        
	        scanner.close();
	        
	        System.out.println(stringBuffer.toString());
	        
	        inputStream.close();
	       
	        String content = "The Moon is an astronomical body orbiting Earth and is the planet's only natural satellite" +
			         " \nThe Moon is thought to have formed about 4.51 billion years ago, not long after Earth" + 
			         " \nAfter the Sun, the Moon is the second-brightest celestial object regularly "
			         + "visible in Earth's sky." +
			         " \nthe Moon covers the Sun nearly precisely during a total solar eclipse" ;
	        
	        InputStream stringInputStream = new ByteArrayInputStream(content.getBytes());
	        int data;
	        
	        while((data = stringInputStream.read()) != -1) {
	     	   
	            System.out.print((char) data);
	        }
	        
	        stringInputStream.close();
	    }
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class StringInputStreamConversion {
	
    public static void main(String[] args) throws IOException {
 	   
        InputStream inputStream = new FileInputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
        
        Scanner scanner = new Scanner(inputStream);
        StringBuffer stringBuffer = new StringBuffer();
        
        while(scanner.hasNext()) {
     	   
            stringBuffer.append(scanner.nextLine());
        }
        
        scanner.close();
        
        System.out.println(stringBuffer.toString());
        
        inputStream.close();
       
        String content = "The Moon is an astronomical body orbiting Earth and is the planet's only natural satellite"
		         + " \nThe Moon is thought to have formed about 4.51 billion years ago, not long after Earth"
		         + " \nAfter the Sun, the Moon is the second-brightest celestial object regularly "
		         + "visible in Earth's sky."
		         + " \nthe Moon covers the Sun nearly precisely during a total solar eclipse" ;
        
        InputStream stringInputStream = new ByteArrayInputStream(content.getBytes());
        int data;
        
        while((data = stringInputStream.read()) != -1) {
     	   
            System.out.print((char) data);
        }
        
        stringInputStream.close();
    }
}
