/* 
Requirement:
    write a program to print difference of two numbers using lambda expression and the single method interface
    
Entity:
    LambdaDifference
    Difference
    
Function Declaration:
    public static void main(String[] args) {}
    public int difference(int a,int b)
    
Jobs To Be Done:
    1) Create an interface LambdaDifference and declare a method difference of two parameters.
    2) In the main method lambda expression is used to find the difference of two numbers.
    3) Print the difference of two numbers.
    
pseudo code:
	interface LambdaDifference {
		
		public int difference(int a,int b);
	}
	
	public class Difference {
		
		public static void main(String[] args) {
			
			LambdaDifference lambdaDifference = (x, y) -> x - y;
			System.out.println(lambdaDifference.difference(10,8));
		}
	}
 */

package com.kpr.training.java.exercise3.lambdaexpression;

interface LambdaDifference {
	
	public int difference(int a,int b);
}

public class Difference {
	
	public static void main(String[] args) {
		
		LambdaDifference lambdaDifference = (x, y) -> x - y;
		System.out.println(lambdaDifference.difference(10,8));
	}
}
