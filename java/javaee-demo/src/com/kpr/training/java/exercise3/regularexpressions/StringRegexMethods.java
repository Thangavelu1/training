/*
Requirement:
	To write a program for Java String Regex Methods
	
Entity:
	StringRegexMethods
	
Function Declaration:
	public static void main(String[] args) {}
	
Jobs To Be Done:
	1) Declare the variable text and store the text "hi i am steve and i am from america".
	2) use match() method to match name
	3) use split() method and split into two substrings
	4) use replaceFirst() method and replace the first matching one in the text
	5) use replaceAll() method to replace all the string matches with the given one
	6) print all the results.
	
Pseudo code:
	public class StringRegexMethods {
	
		public static void main(String[] args) {
		
			String text = "hi i am steve and i am from america";
			// use match() method to match name
		
			// use split() method and split into two substrings
			// print the result
		
			// use replaceFirst() method and replace the first matching one in the text
			// print the result
		
			// use replaceAll() method to replace all the string matches with the given one
			// print the result
		}
	}
 */

package com.kpr.training.java.exercise3.regularexpressions;

import java.util.Arrays;

public class StringRegexMethods {
	
	public static void main(String[] args) {
		
		String text = "hi i am steve and i am from america";
		boolean matches = text.matches(".*steve.*");
		System.out.println(matches);
		
		String[] name = text.split("and");
		System.out.println(Arrays.toString(name));
		
		String name1 = text.replaceFirst("steve", "a student");
		System.out.println(name1);
		
		String replace = text.replaceAll("steve", "students");
		String replace1 = replace.replaceAll("i am", "we are");
		System.out.println(replace1);
	}
}
