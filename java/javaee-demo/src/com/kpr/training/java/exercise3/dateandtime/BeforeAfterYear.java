/*
Requirement:
    To write a Java program to get the dates 1 year before and after today.
      
Entity:
    BeforeAfterYear
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1) Create a reference for Calendar as calendar.
    2) Invoke the method getInstance() to get the current time, date and store it in the calendar.
    3) Invoke the method add(Calendar.YEAR, -1) to get one year before date and time by invoking getTime() method.
    4) Print the result.
    5) Invoke the method add(Calendar.YEAR, 2) to get one year after date and time by invoking getTime() method.
    6) Print the result.
    
Pseudo code:
public class BeforeAfterYear {
    
    public static void main(String[] args) {
    	
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calendar.getTime());
    }
}
*/

package com.kpr.training.java.exercise3.dateandtime;

import java.util.Calendar;

public class BeforeAfterYear {
    
    public static void main(String[] args) {
    	
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calendar.getTime());
    }
}
