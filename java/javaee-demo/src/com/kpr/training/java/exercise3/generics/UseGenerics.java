/*
Requirement: 
	What will be the output of the following program?
	
	    public class UseGenerics {
	        public static void main(String[] args){  
	            MyGen<Integer> m = new MyGen<Integer>();  
	            m.set("merit");
	            System.out.println(m.get());
	        }
	    }
	    class MyGen<T> {
	        T var;
	        void  set(T var) {
	            this.var = var;
	        }
	        T get() {
	            return var;
	        }
	    }

 */

package com.kpr.training.java.exercise3.generics;

/*
Output:
	Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
	The method set(Integer) in the type MyGen<Integer> is not applicable for the arguments (String)
	at com.kpr.training.generics.UseGenerics.main(UseGenerics.java:39)
*/

/* 
It gives a compile time error because while creating the reference the generic type is given as Integer,
	but String is passed as argument in set method.
*/