/*
Requirement:
      To filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

Entity:
      FilterPerson
      Person
    
Function Declaration:
      public static void main(String[] args) {}
        
Jobs To Be Done:
      1) Create an object as roster for the method createRoster().
      2) Create an ArrayList as type string.
      3) Check the each person's gender is equal to Male
          3.1) if it is male store the person name in ArrayList.
      4) Print the first element in ArrayList as first filtered person.
      5) Print last element in the ArrayList as last filtered person.
      6) Print the random filtered person.
      
Pseudo code:
	public class FilterPerson {

	    public static void main(String[] args) {
	        List<Person> roster = Person.createRoster();
	        ArrayList<String> name = new ArrayList<>();
	        for (Person p : roster) {
	            if(gender==male){
	                //add name in arraylist
	             }
	        }
	        //get the size of array
	        System.out.println("The First Filtered Person is " + name.get(0));
	        System.out.println("The Last Filtered Person is " + name.get(size - 1));    
	        Random random = new Random();
	        //get the random index
	        System.out.println("The Random Filtered Person is " + name.get(index));
        }
   }            
*/
package com.kpr.training.java.exercise3.collections.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FilterPerson {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        
        for (Person person : roster) {
            if (person.getGender() == Person.Sex.MALE) {
                name.add(person.getName());
            }
        }
        
        int size = name.size();
        System.out.println("The First Filtered Person is " + name.get(0));
        System.out.println("The Last Filtered Person is " + name.get(size - 1));

        Random random = new Random();
        int index = random.nextInt(size);
        System.out.println("The Random Filtered Person is " + name.get(index));

    }
}