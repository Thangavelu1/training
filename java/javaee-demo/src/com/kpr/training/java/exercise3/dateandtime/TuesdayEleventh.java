/* 
Requirement:
	Write an example that tests whether a given date occurs on Tuesday the 11th.
Entity: CheckTuesday

Function Declaration: public static void main(String[] args);

Jobs to be done:
	1) Assign the input date to date variable which is of type LocalDate.
	2) Check if the day  of the month in the given date is 11 and day of week is tuesday.
		2.1) Print "The given date is 11 and day is tuesday.
	    2.2) 0therwise print "Day is not tuesday".

PseudoCode:

public class TuesdayEleventh {

	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2015, 8, 11);
		
		if((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2)) {
			
			System.out.println("Given date is 11 and day is tuesday");
		} 
		else {
			
			System.out.println("Day is not tuesday");
		}
	}
}

 * */
package com.kpr.training.java.exercise3.dateandtime;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class TuesdayEleventh {

	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2015, 8, 11);
		
		if((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2)) {
			
			System.out.println("Given date is 11 and day is tuesday");
		} 
		else {
			
			System.out.println("Day is not tuesday");
		}
	}
}