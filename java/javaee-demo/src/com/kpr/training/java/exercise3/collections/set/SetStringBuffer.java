/* 
Requirement:
 	java program to demonstrate insertions and string buffer in tree set.
 	
Entity:
 	SetStringBuffer
 	
Function Declaration:
    public static void mian(String[] args) {} 
    
Jobs To Be Done:
    1.create a treeset
    2.Add the elements to the treemap as stringbuffer
 
Pseudo code:
    public class StringBUfferInsert {
   
        public static void main(String[] args) {
      
      	    Treeset<StringBuffer> treeset = new TreeSet<StringBuffer>();
            // Add elements to TreeSet as StringBuffer
        }
     }
 */

package com.kpr.training.java.exercise3.collections.set;

import java.util.TreeSet;

public class SetStringBuffer {
	
	public static void main(String[] args) {
		
		TreeSet<StringBuffer> treeset = new TreeSet<StringBuffer>();
		
		treeset.add(new StringBuffer("apple"));
		treeset.add(new StringBuffer("banana"));
		treeset.add(new StringBuffer("carrot"));
		treeset.add(new StringBuffer("dragon"));
		treeset.add(new StringBuffer("grape"));	
	}
}