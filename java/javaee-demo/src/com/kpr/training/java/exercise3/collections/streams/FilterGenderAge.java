/*
Requirement:
	Write a program to filter the Person, who are male and age greater than 21
	
Entity:
	FilterGenderAge
	Person
	
Function Declaration:
	public static void main(String[] args) {}
    public static List<Person> createRoster()
    public Sex getGender() 
    public int getAge() 
    
Jobs To Be Done:
	1) Invoke the method createRoaster and create an object
    2) Store the list in roster.
    3) Iterate the list using stream
        3.1) The name is filtered if gender is male and age is greater than 21.
        3.2) Print the name using for Each loop.
        
pseudo code:
 	public class FilterGenderAge {
	 
		public static void main(String[] args) {
	       
		    List<Person> roster = Person.createRoster();  
	        
	        use stream to iterate the list.
	        filter(Gender == Male && Age >21 )
	        print the person name using getName() method.
	        
	   }
    }
*/

package com.kpr.training.java.exercise3.collections.streams;

import java.util.List;

public class FilterGenderAge {
	
	public static void main(String[] args) {
		
        List<Person> roster = Person.createRoster();
        
        roster.stream().filter(person -> person.getGender() == Person.Sex.MALE)
        			   .filter(person1 -> person1.getAge() > 21)
        			   .forEach(element -> System.out.println(element.getName()));
    }
}
