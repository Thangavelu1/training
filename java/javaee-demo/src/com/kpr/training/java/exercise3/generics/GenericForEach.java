/*
Requirement :
    Write a program to demonstrate generic - for loop, for list, set and map

Entity:
    GenericForEach
 
Function Declaration:
    public static void main(String[] args ) {}
    
Jobs To be Done :
    1) Create a set type String and store the String values in the variable wish.
    2) Print the set using for each loop.
    3) Create a list type Integer and store the numbers in the variable list.
    4) Print the list using for each loop.
    5) Create a map in that key of type Integer and value of type String and store in the variable map.
    6) Print the map using for each loop.

pseudo code:

public class GenericForEach {
    
    public static void main(String[] args) {
        
        //set using for each loop
        Set<String> set = new HashSet<>();
        //add elements to the set
        //iterate using for each loop
        
        //list using for each loop
        List<Integer> list = new ArrayList<>();
        //add elements to the list
        //iterate using for each loop
         
        //map using for each loop
        HashMap<String , String> map = new HashMap<>();
        //add key and value pair to the map
        //iterate using for each loop
    }
}
 */

package com.kpr.training.java.exercise3.generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class GenericForEach {
    
    public static void main(String[] args) {
        
        //set using for each loop
        Set<String> wish = new LinkedHashSet<>();
        
        wish.add("Good Morning");
        wish.add("Good Afternoon");
        wish.add("Good Evening");
        wish.add("Good Night");
        
        for (String word : wish) {
        	
            System.out.println("Hi, " + word);
        }
        
        System.out.println();
        
        //list using for each loop
        List<Integer> list = new ArrayList<>();
        
        list.add(100);
        list.add(200);
        list.add(300);
        list.add(400);
        
        for (Integer number : list) {
        	
            System.out.println("the number is " + number);
        }
        
        System.out.println();
        
        //map using for each loop
        HashMap<Integer, String> map = new HashMap<>();
        
        map.put(1, "Alex");
        map.put(2, "Ben");
        map.put(3, "Chris");
        map.put(4, "Dany");
        
        for (Integer i : map.keySet()) {
        	
            System.out.println("key: " + i + " value: " + map.get(i));
        }
    }
}
