/*
Requirement: 
		Read a file using java.nio.Files using Paths.

Entity:
		NioFiles

Function Declaration:
		public static void main(String[] args) {}

Jobs To Be Done:
		1) Create a reference for Paths.
		2) Add all Lines in the file to the list.
		3) For each file name in the list.
			3.1) Print all lines.

PseudoCode:
	public class NioFiles {
	
		public static void main(String[] args) throws IOException {
			
			Path path = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
					+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
			List<String> lines = Files.readAllLines(path);
			
			for (String line : lines) {
				
				System.out.println(line);
			}
		}
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NioFiles {

	public static void main(String[] args) throws IOException {
		
		Path path = Paths.get("C:\\1dev\\database\\training\\java\\javaee-demo\\"
				+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
		List<String> lines = Files.readAllLines(path);
		
		for (String line : lines) {
			
			System.out.println(line);
		}
	}
}
