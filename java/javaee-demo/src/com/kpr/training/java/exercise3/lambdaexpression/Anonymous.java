/*
Requirement: 
    To convert the following anonymous class into lambda expression.
		interface CheckNumber {
		    public boolean isEven(int value);
		}
		
		public class EvenOrOdd {
		    public static void main(String[] args) {
		        CheckNumber number = new CheckNumber() {
		            public boolean isEven(int value) {
		                if (value % 2 == 0) {
		                    return true;
		                } else return false;
		            }
		        };
		        System.out.println(number.isEven(11));
		    }
		}
Entity:
     CheckNumber
     Anonymous
     
Function Declaration :
     boolean isEven(int number)
     public static void main(String[] args) {}
     
Jobs To Be Done:
     1) Get the input from the user.
     2) Create a CheckNumber interface and create a isEven method.
     3) Consider the class AnonymousToLambda.
     4) Create a scanner object and a variable num and getting a input.
     5) Create check reference and defining a lambda function.
     6) Print the result using isEven method.
     
Pseudo code:
	interface CheckNumber {
	    
	    boolean isEven(int number);
	}
	
	public class Anonymous {
		
	   public static void main(String[] args) {
	        
	        Scanner scanner = new Scanner(System.in);
	        System.out.println("Enter the number ");
	        int num = scanner.nextInt();
	        
	        CheckNumber check = (int number) -> (number % 2 == 0) ? true : false;
	        System.out.println(check.isEven(num));
	        
	        scanner.close();
	    }
	}
 */

package com.kpr.training.java.exercise3.lambdaexpression;

import java.util.Scanner;

interface CheckNumber {
    
    boolean isEven(int number);
}

public class Anonymous {
	
   public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number ");
        int num = scanner.nextInt();
        
        CheckNumber check = (int number) -> (number % 2 == 0) ? true : false;
        System.out.println(check.isEven(num));
        
        scanner.close();
    }
}
