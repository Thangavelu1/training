/*
Requirement: 
	Write some String content using Writer.

Entity:
	StringWritter

Function Declaration:
	public static void main(String[] args) {}

Jobs To Be Done:
	1) Create a reference for FileReader with file as constructor argument.
    2) Till the end of the file
        2.1) Read the content of the file.
        2.2) Print the content of the file.
    3) Close the created input stream.

Pseudo code:
	public class StringWritter {
	
		public static void main(String[] args) throws IOException {
			
			Writer write = new FileWriter("C:\\1dev\\database\\training\\java\\javaee-demo\\src"
					+ "\\com\\kpr\\training\\java\\exercise3\\advanced\\putData.txt");
			
			String content = "write a file";
			
			write.append(content);
			write.close();
			
			System.out.println("success");
		}
	}
 */

package com.kpr.training.java.exercise3.advanced;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class StringWritter {

	public static void main(String[] args) throws IOException {
		
		Writer write = new FileWriter("C:\\1dev\\database\\training\\java\\javaee-demo\\src"
				+ "\\com\\kpr\\training\\java\\exercise3\\advanced\\putData.txt");
		
		String content = "write a file";
		
		write.append(content);
		write.close();
		
		System.out.println("success");
	}
}
