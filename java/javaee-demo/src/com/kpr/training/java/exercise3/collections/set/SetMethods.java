/* 
Requirement:
	    Create a set
	 => Add 10 values
	 => Perform addAll() and removeAll() to the set.
	 => Iterate the set using 
	     - Iterator
	     - for-each
	    Explain the working of contains(), isEmpty() and give example.
    
Entity:
    SetMethods
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a Generic Set of String as  hashSet, hashSet2, hashSet3.
    2) Add 10 elements to hashSet.
    3) Add 4 elements to hashSet2.
    4) Add 3 elements to hashSet3.
    5) Invoke addAll() method to add hashSet2 to hashSet.
    6) Invoke removeAll() method to remove all values of hashSet in hashSet3.
    7) Using iterator to iterate the elements and print the elements.
    8) Using for each loop to print the elements.
    9) Invoke contains() method to find whether the element present or not. 
    10) Invoke isEmpty() method to find the set is empty or not.
    
Pseudo code:
	public class SetMethods {
		
	    public static void main(String[] args) {
			
			Set<String> hashSet = new HashSet<String>();
			
			// Add 10 elements to hashSet
			
			Set<String> hashSet2 = new HashSet<String>();
			
			// Add 4 elements to hashSet2
			
			Set<String> hashSet3 = new HashSet<String>();
			
			// Add 3 elements to hashSet3
			
			// Invoke addAll() method and print the result 
			
			// Invoke removeAll() method and print the result
			
			// Using Iterator to print the element
			
			// Using for each loop to print the element
			
			// Invoke contains() method to find whether the element present or not.
			
			// Invoke isEmpty() method to find the set is empty or not.
			
		}
	}
 */

package com.kpr.training.java.exercise3.collections.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetMethods {
	
    public static void main(String[] args) {
		
		Set<String> hashSet = new HashSet<String>();
		
		hashSet.add("Hi");
		hashSet.add("I");
		hashSet.add("am");
		hashSet.add("Thangavelu");
		hashSet.add("and");
		hashSet.add("I");
		hashSet.add("am");
		hashSet.add("from");
		hashSet.add("Tiruppur");
		hashSet.add("Tamil Nadu");
		
		Set<String> hashSet2 = new HashSet<String>();
		
		hashSet2.add("My");
		hashSet2.add("department");
		hashSet2.add("is");
		hashSet2.add("EEE");
		
		Set<String> hashSet3 = new HashSet<String>();
		
		hashSet3.add("am");
		hashSet3.add("my");
		hashSet3.add("I");
		
		//addAll
		hashSet.addAll(hashSet2);
		System.out.println(hashSet);
		
		//removeAll
		hashSet3.removeAll(hashSet);
		System.out.println(hashSet3);
		
		//Iterator
		Iterator<String> iterator = hashSet.iterator();
		
		System.out.println("Iterator : ");
		while(iterator.hasNext()) {
			String s = iterator.next();
			System.out.print(s + " ");
		}
		
		//for each loop
		System.out.println("\nfor each loop : ");
		for (String s : hashSet) {
			String e = (String) s;
			System.out.print(e + " ");
		}
		
		//contains()
		System.out.println("\nContains Method :");
		System.out.println(hashSet.contains("Thangavelu"));
		System.out.println(hashSet2.contains("Tiruppur"));
		
		//isEmpty()
		System.out.println("isEmpty Method :");
		boolean isEmpty = hashSet.isEmpty();
		System.out.println(isEmpty);
		boolean isEmpty1 = (hashSet3.size() == 0);
		System.out.println(isEmpty1);
		
	}
}
