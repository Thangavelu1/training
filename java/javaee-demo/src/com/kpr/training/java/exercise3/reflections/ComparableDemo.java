package com.kpr.training.java.exercise3.reflections;

//class Node<T> implements Comparable<T> {
//    public int compareTo(T object) { /* ... */ //}
          // ...
//}
/*
Will the following code compile? If not, why?

Answer: Yes.
Node<String> node = new Node<>();
Comparable<String> comparator = node;
*/