/*
Requirement:
    write a program for email-validation?

Entity:
    EmailValidation

Function Declaration :
    public static boolean validMail(String mailId)
    public static void main(String[] args) 

Jobs To Be Done:
    1)Create an object for the scanner
    1.Get mail Id From user and store it in mailId.
    2.Invoke the validMail method From the class EmailValidationDemo 
      3.1) Pass the user mailId as parameter
      3.2) Declare a pattern for mailId and store it in pattern
      3.3) Create a Matcher named matcher and store the match of the pattern against the id.
      3.4) If the pattern it will return true otherwise it will return false.
    4)Returned value is printed.

pseudo code:

public class EmailValidation {
    
    public static boolean validMail(String id) {
     
        Declare a pattern for mailId and store it in pattern    
    
        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mailid ");
        String mailId = scanner.next();
        System.out.println(validMail(mailId));
        scanner.close();
    }
}

 */
package com.kpr.training.java.exercise3.regularexpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
    
    public static boolean validMail(String id) {
    	
    	String regex = "^[a-zA-Z0-9_+&-]+(?:\\."+"[a-zA-Z0-9_+&-]+)*@"+"(?:[a-zA-Z0-9-]+\\.)+[a-z"+"A-Z]{2,7}$";
    	
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mailid ");
        
        String mailId = scanner.next();
        System.out.println("pattern matched:"+validMail(mailId));
        
        scanner.close();
    }
}
