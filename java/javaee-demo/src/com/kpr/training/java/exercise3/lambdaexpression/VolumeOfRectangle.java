/*
Requirement:
    Write a program to print the volume of  a Rectangle using lambda expression.
    
Entity:
    LambdaVolumeRectangle
    VolumeOfRectangle
    
Function Declaration:
    public static void main(String[] args) {}
    public int volume(int l,int b,int h)
    
Jobs To Be Done:
    1) Create an interface LambdaRectangle and declare a method difference of three parameters(length,breadth,height).
    2) In the main method lambda expression is used to find the volume of Rectangle.
    3) Print the Volume of the Rectangle.
    
Pseudo code:
  	interface LambdaRectangle {
  	
		public int volume(int length,int breath,int height);
  	}

  	public class VolumeOfRectangle {

		public static void main(String[] args) {
			
			//create lambda expression which returns volume.
			System.out.print("volume of rectangle:");
			System.out.println(lambdaRectangle.volume(10, 8, 10));
    	}
	}
*/

package com.kpr.training.java.exercise3.lambdaexpression;

interface LambdaRectangle {
	
	public int volume(int length, int breadth, int height);
}

public class VolumeOfRectangle {
	
	public static void main(String[] args) {
		
		LambdaRectangle lambdaRectangle = (int x, int y, int z) -> x * y * z;
		
		System.out.println(lambdaRectangle.volume(10, 8, 10) + " " + "cubic.cm");
	}
}
