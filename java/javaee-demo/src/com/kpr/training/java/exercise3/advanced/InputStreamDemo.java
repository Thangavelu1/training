/*
Requirement:
    To read a file using InputStream.
   
Entity:
    InputStreamDemo
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create a reference for FileInputStream with file as constructor argument.
    2) Till the end of the file
        2.1) Read the content of the file.
        2.2) Print the content of the file.
    3) Close the created input stream.

Pseudo code:
	public class InputStreamDemo {
	
	    public static void main(String[] args) throws IOException {
	    	
	        InputStream inputStream = new FileInputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
	        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
	        int data = inputStream.read();
	        
	        while(data != -1) {
	        	
	            System.out.print((char) data);
	            data = inputStream.read();
	        }
	        inputStream.close();
	    }
	}
 */
 
package com.kpr.training.java.exercise3.advanced;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamDemo {

    public static void main(String[] args) throws IOException {
    	
        InputStream inputStream = new FileInputStream("C:\\1dev\\database\\training\\java\\javaee-demo\\"
        		+ "src\\com\\kpr\\training\\java\\exercise3\\advanced\\getData.txt");
        int data = inputStream.read();
        
        while(data != -1) {
        	
            System.out.print((char) data);
            data = inputStream.read();
        }
        inputStream.close();
    }
}
