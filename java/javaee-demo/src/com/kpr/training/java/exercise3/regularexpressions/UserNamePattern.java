/*
Requirement:
     create a username for login website which contains
     8-12 characters in length
     Must have at least one uppercase letter
     Must have at least one lower case letter
     Must have at least one digit
     
Entity:
    UserNamePattern
    
Function Declaration:
     public static boolean validUserName(String UserName);
     public static void main(String[] args) {}
     
Jobs to Done:
    1) Get the input from the user and store it in the variable userName.
    2) Invoke the method validUserName() and pass the word as parameter.
    3) Check if the userName matches
    	3.1) Print "Username is valid"
    4) else print "Username is invalid"
    5) Create a method validUserName()
     	5.1) Check whether the name is null.
     	5.2) Create a regex pattern for the given conditon and store it in the variable regex as type String
     	5.3) Compile the regex pattern Using Pattern
     	5.4) Match the pattern with input using Matcher
     	5.5) Return the boolean output
        
Pseudo code:
public class UserNamePattern {

    public static boolean validUserName(String userName) {
    	
    	if (userName == null) {
    	
            return false;
        }
    	
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter the UserName ");
        
        String userName = scanner.next();
        
        if (validUserName(userName)) {
        	
        	System.out.print("Username is valid");
        }
        else {
        	
        	System.out.print("Username is invalid");
        }   
        scanner.close();
    }
}
 */

package com.kpr.training.java.exercise3.regularexpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNamePattern {

    public static boolean validUserName(String userName) {
    	
    	if (userName == null) {
            return false;
        }
    	
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,12}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter the UserName ");
        
        String userName = scanner.next();
        
        if (validUserName(userName)) {
        	
        	System.out.print("Username is valid");
        }
        else {
        	
        	System.out.print("Username is invalid");
        }   
        scanner.close();
    }
}
