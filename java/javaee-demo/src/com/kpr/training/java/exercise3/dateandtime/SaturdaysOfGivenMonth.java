/*
Requirement:
	Write a Java program for a given month of the current year, lists all of the Mondays in that month.
    
Entity:
    SaturdaysOfGivenMonth
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs to be done:
    1) Get the month from the user for which list of Mondays to be printed and store it in String variable givenMonth.
    2) Create a reference for Month as month that stores the upper case of givenMonth. 
    3) A reference is created for LocalDatae as localDate that stores for the current year and for
       	given month the date of first SATURDAY in that month.
    4) Create an another instance for Month as anotherMonth which stores the date of the given month.
    5) Check whether the anotherMonth and month are equal
        5.1) if condition satisfies print the date.
        	5.1.1) Get all the next Saturday date and print it

Pseudo code:
public class SaturdaysOfGivenMonth {
    
    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the month : ");
        String givenMonth = scanner.nextLine();
        
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.println("Month : " + month);
        
        LocalDate localDate = Year.now()
        						  .atMonth(month)
        						  .atDay(1)
                                  .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        Month anotherMonth = localDate.getMonth();
        
        
        while(anotherMonth == month) {
        	
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            anotherMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
*/

package com.kpr.training.java.exercise3.dateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class SaturdaysOfGivenMonth {
    
    public static void main(String[] args) {
    	
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the month : ");
        String givenMonth = scanner.nextLine();
        
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.println("Month : " + month);
        
        LocalDate localDate = Year.now()
        						  .atMonth(month)
        						  .atDay(1)
                                  .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        
        Month anotherMonth = localDate.getMonth();
        
        
        while(anotherMonth == month) {
        	
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            anotherMonth = localDate.getMonth();
        }
        
        scanner.close();
    }
}
