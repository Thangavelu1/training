/*
Requirement:
 	To Create a program using HttpUrlconnection in networking.

Entity: 
	HttpsUrlConnectionDemo

Function Declaration: 
	public static void main(String[] args) {}

Jobs to be done: 
	1) Create a url.
	2) Create HttpUrlConnection using the created url.
	3) Find the information of url header and print it.
					
PseudoCode:
	public class HttpUrlConnectionDemo {
		
		public static void main(String[] args) throws IOException {    
			
			URL url = new URL("http://www.javatpoint.com/java-tutorial");    
			HttpURLConnection huc = (HttpURLConnection)url.openConnection();  
			for(int i = 1 ;i <= 8 ;i++) {  
				System.out.println(huc.getHeaderFieldKey(i) + " = " + huc.getHeaderField(i));  
			}  
			huc.disconnect(); 
		}
	}
 */

package com.kpr.training.java.exercise3.networking;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrlConnection {
	
	public static void main(String[] args) throws IOException {    
		
		URL url = new URL("http://www.tutorialspoint.com/java-tutorial");
		
		HttpURLConnection connect = (HttpURLConnection)url.openConnection();  
		
		for(int i = 1 ;i <= 8 ;i++) {
			
			System.out.println(connect.getHeaderFieldKey(i) + " = " + connect.getHeaderField(i));  
		}  
		connect.disconnect(); 
	}
}
