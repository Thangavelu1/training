/* 
Requirement:
    Write a program to perform the following operations in Properties.
        i) add some elements to the properties file.
        ii) print all the elements in the properties file using iterator.
        iii) print all the elements in the properties file using list method.
    
Entity:
    PropertiesOperations
  
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object for properties.
    2) Add elements to the property as (key - value) in properties.
    3) Create a object for FileOutputStream as output for properties file.
    4) Store the properties in FileOutputStream as properties file.
    5) Print the properties stored successful.
    6) Invoke properties file and assign it to the iterator with type reference.
    7) For each element in the property file 
        7.1) get key and values and print the element.
    8) Print the element by using list method.
  
Pseudo code:

    class PropertiesOperations {
    
        public static void main(String[] args) throws IOException{ 
            Properties properties = new Properties();
            // Add the element to the properties.
            OutputStream output = new FileOutputStream("Filename.properties");
            // Store the property element to the Outputstream.
            System.out.println("Properties stored");
        
            System.out.println("Printing all the elements in the properties file");
            
            // Iterate the elements using iterator 
             
            properties.list(System.out);
        }
    }
*/

package com.kpr.training.java.exercise3.collections.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Properties;

public class PropertiesOperations {

    public static void main(String[] args) throws IOException{
    	
        Properties properties = new Properties();
        
        properties.setProperty("Cricket", "Virat Kohli");
        properties.setProperty("Foot Ball", "Cristiano Ronaldo");
        properties.setProperty("Badminton", "P. V. Sindhu");
        properties.setProperty("Basketball", "LeBron James");

        OutputStream output = new FileOutputStream("SportsPlayers.properties");
        
        properties.store(output, "Sports and Players");
        
        System.out.println("Properties stored");

        System.out.println("All the elements in the properties file");
        
        Iterator<Object> iterator = properties.keySet().iterator();
        
        while (iterator.hasNext()) {
        	
            String key = (String) iterator.next();
            String value = properties.getProperty(key);
            System.out.println(key + " = " + value);
        }
        
        properties.list(System.out);
    }
}

