/* 
Requirement:
     Write a program to find the average age of all the Person in the person List

Entity:
     AverageAge
     Person

Function Declaration:
     public static void main(String[] args)

Jobs To Be Done:
    1) Create an object as roster for the method createRoster().
    2) Get the average age of the persons and store it in the variable average which type is double.
    3) Print the average age.
    
Pseudo Code:
	class AverageAge {
		
		public static void main(String[] args) {
		
	        List<Person> roster = Person.createRoster();
	        // Get the average age and store it in average
	        System.out.println("Average age : " + average);
		
	    }
	}

*/

package com.kpr.training.java.exercise3.collections.streams;

import java.util.List;

public class AverageAge {

    public static void main(String[] args) {
    	
        List<Person> roster = Person.createRoster();
        
        double average = roster.stream().mapToInt(person -> person.getAge())
        								.average()
        								.getAsDouble();
        
        System.out.println("Average age : " + average);
    }
}