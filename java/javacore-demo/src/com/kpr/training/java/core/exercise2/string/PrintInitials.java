/*
Requirement:
    To print the intials from the name.
    
Entity:
    PrintInitials
    
Function Declaration:
    public static void main(String[] args).
    
Jobs to be Done:
    1) Declare and assign the variable myName as "R.Thangavelu".
    2) Create an object for StringBuffer as myInitials
    3) Invoke the method length() to get the length of the string.
    4) Iterate the string using for loop
    	4.1) Check if the character is Uppercase
    		4.1.1) Append the character to the myInitials.
    5) Print the Uppercase letters of the variable as Initials.
*/

package com.kpr.training.java.core.exercise2.string;

public class PrintInitials {
    
    public static void main(String[] args) {
        
        String myName = "R.Thangavelu";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();
        
        for(int i = 0;i < length;i++) {
            
            if(Character.isUpperCase(myName.charAt(i))) {
                
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are :" + myInitials);
    }
}
