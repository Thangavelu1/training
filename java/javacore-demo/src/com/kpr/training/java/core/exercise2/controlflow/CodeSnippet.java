package com.kpr.training.java.core.exercise2.controlflow;

class CodeSnippet {
    
    public static void main (String[] args) {
        
        int aNumber = 3;
        
        if (aNumber >= 0) {
            
            if (aNumber == 0) {
                
                System.out.println("first string");
            }
            else {
            
                System.out.println("second string");
            }       
        }
        System.out.println("third string");
    }
}

/*
OUTPUT:
second string
third string
*/
