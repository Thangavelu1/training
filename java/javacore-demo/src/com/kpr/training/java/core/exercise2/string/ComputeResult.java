/*
Requirement:
    In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');
            
    //1   result.setCharAt(0, original.charAt(0));
    //2   result.setCharAt(1, original.charAt(original.length()-1));
    //3   result.insert(1, original.charAt(4));
    //4   result.append(original.substring(1,4));
    //5   result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }
    
Entity:
    ComputeResult
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Declare the variable original as type String and store the given String value.
    2) Create an instance for StringBuilder and pass parameter "hi".
    3) Print the result for the above question.
*/

package com.kpr.training.java.core.exercise2.string;

public class ComputeResult {
    
    public static void main(String[] args) {
        
        String original = "software";
        StringBuilder result = new StringBuilder("hi");

/*1*/   result.setCharAt(0, original.charAt(0));
        System.out.println(result);
        
/*2*/   result.setCharAt(1, original.charAt(original.length()-1));
        System.out.println(result);
        
/*3*/   result.insert(1, original.charAt(4));
        System.out.println(result);
        
/*4*/   result.append(original.substring(1,4));
        System.out.println(result);
        
        int index = original.indexOf('a');
/*5*/   result.insert(3, (original.substring(index, index+2) + " ")); 
        System.out.println(result);
    }
}

/*
Answer:

si
se
swe
sweoft
swear oft
*/
