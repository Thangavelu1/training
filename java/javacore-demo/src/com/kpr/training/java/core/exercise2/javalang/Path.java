/*
Requirement:
    print the absolute path of the .class file of the current class
    
Entity:
    Path
    
Function Declaration:
    public static void main(final String[] args) {}
    
Jobs To Be Done:
    1) Print the absolute path of the file.
*/

package com.kpr.training.java.core.exercise2.javalang;

public class Path {
    
    public static void main(String[] args) {
    
        System.out.println("current path : " +System.getProperty("user.dir" ));
    }
}