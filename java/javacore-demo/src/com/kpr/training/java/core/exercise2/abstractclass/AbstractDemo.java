/*
Requirement:
    Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
    
Entity:
    abstract class Shape
    Square
    Circle
    AbstractDemo
                                                            
Function Declaration:                                                      
    public Square(double side)                                             
    public abstract void printArea();                                      
    public abstract void printPerimeter();                                 
    public Circle(double radius)                                           
    public static void main(String[] args) {} 
                                 
Jobs To Be Done:                                                           
	1) Get the value of side and radius from the user.                     
	2) Store the value                                                     
		2.1) side in the variable side.                                    
		2.2) radius in the variable radius.
	3) Create an object for
		3.1) Square class and pass the parameter side.
		    3.1.1) Invoke the method printArea() and printPerimeter() for square.
		3.2) Circle class and pass the parameter radius.
			3.2.1) Invoke the method printArea() and printPerimeter() for circle.                
*/

package com.kpr.training.java.core.exercise2.abstractclass;

import java.util.*;

abstract class Shape{
    
    public abstract void printArea();
    public abstract void printPerimeter();
}

class Square extends Shape {
    
    private double side;
    
    public Square(double side) {
    
        this.side = side;
    }
    
    public void printArea() {
    
        System.out.println("area of square = " + (side*side));
    }
    
    public void printPerimeter() {
    
        System.out.println("perimeter of square = " + (4 * side));
    }
}

class Circle extends Shape {
    
    private double radius; 
    
    public Circle(double radius) {
   
        this.radius = radius;
    }  
    
    public void printArea() {
    
        System.out.println("area of circle is = " + (3.14 * radius * radius));
    }
    
    public void printPerimeter() {
    
        System.out.println("perimeter of circle = " + (2 * 3.14 * radius));
    }
}

public class AbstractDemo {
    
    public static void main(String[] args) {
    
		Scanner scanner = new Scanner(System.in);
		
        System.out.print("Enter the lenghth of the side of the square :");
        double side = scanner.nextDouble();
        
        System.out.print("Enter the radius of the circle :");
        double radius = scanner.nextDouble();
        
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
        
        scanner.close();
    }
}