/*
Requirement:
    To Change the following program to use compound assignments
        class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
        }
        
Entity:
    ArithmeticDemo
    
Function Declaration:
    public static void main (String[] args) {}
    
Jobs To Be Done:
	1) Declare the variable result as type int
	2) Use compound assignments for the above program.
	3) Print the results.
*/

package com.kpr.training.java.core.exercise2.operators;

class ArithmeticDemo {

    public static void main (String[] args) {

         int result = 1 + 2; // result is now 3
         System.out.println(result);

         result -= 1; // result is now 2
         System.out.println(result);

         result *= 2; // result is now 4
         System.out.println(result);

         result /= 2; // result is now 2
         System.out.println(result);

         result += 8; // result is now 10
         System.out.println(result);
         
         result %= 7; // result is now 3
         System.out.println(result);
    }
}