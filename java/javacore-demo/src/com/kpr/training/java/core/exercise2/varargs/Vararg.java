/*
Requirement:
    To demonstrate overloading with varArgs
    
Entity:
    Vararg
    VariableArgument
    
Function declaration: 
    public static void main(String[] args) {}
    public void printNumbers(int... number) {}
    
Jobs to be done:
    1) Create an instance for VariableArgument.
    2) Invoke the method printNumbers and pass the parameter.
    3) Create another class VariableArgument
    	3.1) Declare the method printNumbers and print the result using for each loop.
    4) Print the results.
*/

package com.kpr.training.java.core.exercise2.varargs;

class VariableArgument {
    
    public void printNumbers(int... number) {
        
        System.out.println("The numbers are:");
        
        for(int i : number) {
            
            System.out.println(i);
        }
    }
}
public class Vararg {
    
    public static void main(String[] args) {
        
        VariableArgument variableArgument = new VariableArgument();
        variableArgument.printNumbers(1,2,3);
        variableArgument.printNumbers(1,2,3,4,5,6,7,8,9,10);
    }
}