/*
Requirement:
    To check the wrong in the following program.
        public class SomethingIsWrong {
            public static void main(String[] args) {
                Rectangle myRect;
                myRect.width = 40;
                myRect.height = 50;
                System.out.println("myRect's area is " + myRect.area());
            }
        }
        
Entity:
    Rectangle
    
Function Declaration:
    public int area();
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Declare the variable width and height.
    2) Create a method area() which return area of rectangle.
    3) Create an instance for rectangle
    	3.1) Initialize the variable width and height.
    	3.2) Invoke the method area() and store it in the variable areaOfRectangle.
    4) Print the area.
    	
*/

package com.kpr.training.java.core.exercise2.classobject;

public class Rectangle {
    
    public int width;
    public int height;
    
    public int area(){
    	
        return (width * height);
    }
    
    public static void main(String[] args) {
        
        Rectangle rectangle = new Rectangle();
        rectangle.width = 40;
        rectangle.height = 50;
        int areaOfRectangle = rectangle.area();
        
        System.out.println("Area of Rectangle is " + areaOfRectangle);
    }
}