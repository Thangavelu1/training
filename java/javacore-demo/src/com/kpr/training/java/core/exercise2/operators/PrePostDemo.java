/*
Requirement:
    To find why the value "6" is printed twice in a row in the following program
           class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
       
Entity:
    PrePostDemo
*/

package com.kpr.training.java.core.exercise2.operators;

public class PrePostDemo {
    
    public static void main(String[] args) {
        
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
    }
}

/*
Explanation:
    System.out.println(++i);  // "6" (increment the 'i' value before initialization - pre increment)
    System.out.println(i++);  // "6" (increment the 'i' value after initialization - post increment)
    System.out.println(i);    // "7" ('i' value after initialization )
*/