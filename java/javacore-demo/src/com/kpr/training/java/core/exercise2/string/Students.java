package com.kpr.training.java.core.exercise2.string;

/*
Requirement:
    The following code creates one array and one string object. How many references to those objects exist after the code executes?
Is either object eligible for garbage collection?
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;
*/

/*
Solution:

	1) There is one reference to the students array and that array has one reference to the string Peter Parker.
	2) Neither object is eligible for garbage collection.
	3) The array students is not eligible for garbage collection 
		because it has one reference to the object studentName even though that object has been assigned the value null.
	4) The object studentName is not eligible either because students[0] still refers to it.
*/
