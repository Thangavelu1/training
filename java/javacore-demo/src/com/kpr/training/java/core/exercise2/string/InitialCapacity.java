package com.kpr.training.java.core.exercise2.string;

/*
Requirement:
    To find What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
*/

/*
SOLUTION:
    Length of the given String = 26
    Initial capacity of the string = 26 + 16 = 42.
*/
