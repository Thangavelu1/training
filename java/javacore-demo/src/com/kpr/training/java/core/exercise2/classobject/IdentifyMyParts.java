/*
Requirement:
    To find the class and instance Variables in the given program.
    public class IdentifyMyParts {
            public static int x = 7;
            public int y = 3;
        }
Entity:
    class IdentifyMyParts

Function Declaration:
    No function is declared in this program

Jobs To Be Done:
    1) Considering the given class from the question.
    2) Find the class variable(with static keyword) and Instance variable.
*/

package com.kpr.training.java.core.exercise2.classobject;

public class IdentifyMyParts {
    
    public static int x = 7;    // x is class variable
    public int y = 3;           // y is instance variable
    
}
