/*
Requirement:
    How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)

Entity:
    Substring
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Declare the variable String as type String and store the given string.
    2) Print the substring(9,12).
*/

package com.kpr.training.java.core.exercise2.string;

import java.lang.String;

public class SubString {
    
    public static void main(String[] args) {
        
    	String string;
        string = "Was it a car or a cat I saw?";
        String s = string.substring(9,12) ;
        System.out.println(s);
    }
}

/*
SOLUTION:
car
*/
