/*
Requirement:
    What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))
     
Entity:
    EqualCheck
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Create an object wordOne as type String.
    2) Create an object wordTwo as type String.
    3) Print the result for == operator and equals() method.
*/

package com.kpr.training.java.core.exercise2.javalang;

public class EqualCheck { 

    public static void main(String[] args) {
        
        String wordOne = new String("Captain America"); 
        String wordTwo = new String("Captain America"); 
        System.out.println(wordOne == wordTwo); 
        System.out.println(wordOne.equals(wordTwo)); 
    } 
}