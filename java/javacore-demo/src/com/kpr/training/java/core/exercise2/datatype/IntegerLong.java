/*
Requirement:
    Find the value of expression
    What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))
     
Entity:
    IntegerLong
    
Function Declaration:
    public static void main(String[] args) {}
    
Job to be done:
    1) Check the expression is true or false for the above problem.
    2) Print the result for the program.
*/

package com.kpr.training.java.core.exercise2.datatype;

public class IntegerLong {
    
    @SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
        
        System.out.println("Value of the expression - " + Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}

/*
Solution:

    Value of the expression - false
    Both values are same but differ in their data type.
*/