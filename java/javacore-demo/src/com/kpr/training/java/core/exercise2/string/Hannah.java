package com.kpr.training.java.core.exercise2.string;

/*
Requirement:
Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
*/

/*
SOLUTION:
-32
-e
-hannah.charAt(15)
*/
