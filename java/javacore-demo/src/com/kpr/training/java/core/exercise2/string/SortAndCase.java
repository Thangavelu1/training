/*
Requirement:
    sort and print following String[] alphabetically ignoring case.
    Also convert and print even indexed Strings into uppercase
        { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
        
Entity:
    Sort
    
Function Declaration:
    public static void main(String[] args).
Jobs to be Done:
    1. Consider the class Sort for the following program.
    2. To sort the string and ignoring case.
    3. To print the even indexed string in uppercase. 
*/

package com.kpr.training.java.core.exercise2.string;

import java.util.Arrays; 

public class SortAndCase {
    
    public static void main(String[] args) {
        
        String[] city = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(city);
        
        System.out.println(Arrays.toString(city));
        
        for(int i=0; i<city.length; i++) {
            
            if(i%2==0) {
            	
                city[i] = city[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(city));
    }
}