/*
Requirement:
    To compare the enum values using equal method and == operator
    
Entity:
    Days
    EnumDemo
    
Function Declaration:
    valueOf()
    public static void main(String[] args) {}
    
Job to be done:
	1) Create enum Days.
    2) Create instance for Days and pass the parameter WEDNESDAY.
    3) Check the day using equals() method and == operator.
    4) Print the result.
*/

package com.kpr.training.java.core.exercise2.enumexercise;

enum Days {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
}

public class EnumDemo {
    
    public static void main(String[] args) {
       
    	Days day = Days.valueOf("WEDNESDAY");
      
        if(day.equals (Days.SUNDAY)) {
            
            System.out.println("SUNDAY is Government Holiday");
        
        }
        else if(Days.MONDAY == day) {
           
           System.out.println("MONDAY is First Working Day");
        
        }
        else if(Days.TUESDAY == day) {
           
           System.out.println("TUESDAY is Second Working Day");
        
        }
        else if(day.equals(Days.WEDNESDAY)) {
            
            System.out.println("WEDNESDAY is Third Working Day");
       
        }
        else if(Days.THURSDAY == day) {
        
            System.out.println("THURSDAY is Fourth Working Day");
        
        }
        else if(Days.FRIDAY == day) {
        
            System.out.println("FRIDAY is Fifth Working Day");
        
        }
        else if(Days.SATURDAY == day) {
        
            System.out.println("SATURDAY is Sixth Working Day");
        
        }   
    }
}
