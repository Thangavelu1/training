/*
Requirement:
    To create an instance of a class and initializes its two memeber variables with provided values,
    and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
    
Entity:
    NumberHolder
    
Function Declaration:
    public static void main(String[] args) {}
    
Jobs To Be Done:
    1) Declare the variable anInt and aFloat.
    2) Create an instance for class NumberHolder
    	2.1) Initialize the variable anInt and aFloat.
    3)Print the result.
*/

package com.kpr.training.java.core.exercise2.classobject;

public class NumberHolder {
    
    public int anInt;
    public float aFloat;
    
    public static void main(String[] args) {
        
        NumberHolder numberholder = new NumberHolder();
        numberholder.anInt = 10000;
        numberholder.aFloat = 18.5336f;
        System.out.println(numberholder.anInt);   //10000
        System.out.println(numberholder.aFloat);   //18.5336
    }
}