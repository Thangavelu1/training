/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Animal

Function Declaration:
    public void sound()
    public void run()
    public void run(int kilometer)

Jobs to be Done:
    1. Declare the class Animal
    2. Declare the function public void sound() and print the statement.
    3. Declare the function run and print the statement.
    4. Declare the function run(kilometer) and print the statement.
    5. run and run(int kilometer),it shows the examples for overloading
*/

package com.kpr.training.java.core.exercise2.inheritance;

public class Animal {
    
    public void sound() {
        
        System.out.println("Animal producing sound");
    }
    
    public void run() {
        
        System.out.println("the animal is running");
    }
    
    public void run(int kilometer) {
        
        System.out.println("the animal runs without any tired");
    }
}
