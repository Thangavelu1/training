package com.healthcare.training.java.core.exercise1.details;

class Address {
    
    public String doorNo;
    public String street;
    public String city;
    public int pincode;
    
    public Address(String doorNo,String street,String city) {
        
        this.doorNo = doorNo;
        this.street = street;
        this.city = city;
    
    }
    
    public Address() {
        
        doorNo = "50v3";
        street = "Gandhi Nagar";
        city = "Tiruppur";
    }
    
    public void print() {
        
        System.out.println("Door No :" + doorNo);

    }
    
    public static void main(String args[]) {
        
        Address address = new Address("52v3","Thilagar Nagar","Tiruppur");
        Address address1 = new Address();
        address.print();
        address1.print();
    }
}
    