package com.healthcare.training.java.core.exercise1.staff;

import com.healthcare.training.java.core.exercise1.hospital.Hospital;
import com.healthcare.training.java.core.exercise1.hospital.Staff;

class Nurse extends Staff {
    
    public int id;
    protected String type = "Nurse";
    private String degree = "BSN";
    
    public Nurse() {
        
        id = 0;
    }
        
    public Nurse(int a,String b,String c) {
        
        id = a;
        type = b;
        degree = c;
    }
    
    public void print() {
        
        System.out.println("ID :" + id +"," + "Type :" + type + "," + "Degree :" + degree);

    }
        
    public static void main(String args[]) {
        
        Nurse nurse = new Nurse();
        Nurse nurse1 = new Nurse(1,"Nurse","BSN");
        Hospital hospital = new Hospital("Apollo",1990,"Chennai");
        hospital.print();
        nurse.prints();
        nurse.print();
        nurse1.print();
      /*System.out.println("Name :" + hospital.name);
        System.out.println("Year Opened :" + hospital.yearOpened);
        System.out.println("City :" + hospital.city)*/

    }
}
   