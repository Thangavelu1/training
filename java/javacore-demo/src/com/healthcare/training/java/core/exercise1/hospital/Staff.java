package com.healthcare.training.java.core.exercise1.hospital;

public class Staff {
    
    public String name;
    protected int id;
    protected int age;
    public String gender;
    
    public Staff() {        // unparameterized constructor
        
        name = "DEAN";
        id = 1;
        age = 50;
    }
    
    public Staff(String a) {
        
        name = a;
    }
    
    public Staff(String a,int b) {
        
        name = a;
        id = b;
    }
    
    public Staff(String name,int id,int age) {          // parameterized constructor
        
        this.name = name;
        this.id = id;
        this.age = age;
    }
    protected void prints() {       
        
        System.out.println("Name :" + name + "," + "Age :" + age + "," + "Gender :" + gender);
        System.out.println("StaffID :" + id);
    }
    
    public static void main(String args[]) {
        
        Staff staff = new Staff();
        Staff staff1 = new Staff("Thangavelu");
        Staff staff2 = new Staff("Venkat",3);
        Staff staff3 = new Staff("Naveen",4,30);
        staff.prints();
        staff1.prints();
        staff2.prints();
        staff3.prints();
    }
}
