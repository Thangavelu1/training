package com.healthcare.training.java.core.exercise1.staff;

import com.healthcare.training.java.core.exercise1.hospital.Hospital;
import com.healthcare.training.java.core.exercise1.hospital.Staff;

class Doctor extends Staff{
    
    public int doctorId;
    public int experience;
    
    public Doctor() {
        
        doctorId = 0;
        experience = 0;
        
    }
    public Doctor(int i,int e) {
        
        doctorId = i;
        experience = e;
        
    }
    public void print() {
        
        System.out.println("ID :" + doctorId + "," + "Experience :" + experience);
        
    }
    
    public static void main(String args[]) {
       
        Hospital hospital = new Hospital();
        Doctor doctor = new Doctor();
      //Doctor doctor0 = new Doctor("Naveen",4,30);
        Doctor doctor1 = new Doctor(1,5);
      /*System.out.println("Name :" + hospital.name);
        System.out.println("Year Opened :" + hospital.yearOpened);
        System.out.println("City :" + hospital.city);*/
        hospital.print();
        doctor.prints();
        doctor.print();
        doctor.prints();
        doctor1.print();
    }
}
