package com.healthcare.training.java.core.exercise1.hospital;

class Admission {
    
    protected String type;
    protected String date; 
    protected String purpose;
    
    public Admission() {        // unparameterized constructor
        
        type = "NA";
        date = "NA";
        purpose = "NA";
    }
    
    public Admission(String a,String b,String c) {      // parameterized constructor
        
        type = a;
        date = b;
        purpose = c;
    }
    
    public void printa() {
        
        System.out.println("Admission Type : " + type);
        System.out.println("Admission Date : " + date);
        System.out.println("Admission Purpose : " + purpose);
    }
    
    public static void main(String[] args) {
       
        Admission admission = new Admission();
        Admission admission1 = new Admission("Emergency","12-05-2020","First Aid");
        admission.printa();
        admission1.printa();
   }
}

