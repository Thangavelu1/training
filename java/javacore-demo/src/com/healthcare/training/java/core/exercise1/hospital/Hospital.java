package com.healthcare.training.java.core.exercise1.hospital;

public class Hospital {
    
    public String name;
    public int yearOpened;
    protected String city;
    
    public Hospital() {
        
        name = "G.H";
        yearOpened = 1980;
        city = "Tamil Nadu";
    }
    
    public Hospital(String name) {
        
        this.name = name;
    }
    
    public Hospital(String name,int yearOpened) {
        
        this.name = name;
        this.yearOpened = yearOpened;
    }
    
    public Hospital(String name,int yearOpened,String city) {
        
        this.name = name;
        this.yearOpened = yearOpened;
        this.city = city;
    }
    
    public void print() {
        
        System.out.println(name + "," + yearOpened + "," + city);
    }
    
    public static void main(String args[]) {
        
        Hospital hospital = new Hospital();
        Hospital hospital1 = new Hospital("Apollo");
        Hospital hospital2 = new Hospital("Apollo",2000);
        Hospital hospital3 = new Hospital("Apollo",2000,"Chennai");
        hospital.print();
        hospital1.print();
        hospital2.print();
        hospital3.print();
    }
}
