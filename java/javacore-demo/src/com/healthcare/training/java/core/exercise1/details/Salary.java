package com.healthcare.training.java.core.exercise1.details;

class Salary {
    
    protected int netSalary;
    protected int incentive;
    protected int offDay;
    
    public Salary() {
        
        netSalary = 100000;
        incentive = 10000;
        offDay = 5;
        
    }
    
    public void print() {
        
        System.out.println("Salary :" + netSalary);
        System.out.println("Incentive :" + incentive);
        System.out.println("Off Day :" + offDay);
        
    }
    public static void main(String args[]) {
        
        Salary salary = new Salary();
        salary.print();
        
    }
}
