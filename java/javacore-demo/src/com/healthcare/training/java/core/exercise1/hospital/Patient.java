package com.healthcare.training.java.core.exercise1.hospital;

class Patient {
    
    private int id;
    private String name;
    
    public Patient() {
        
        id = 0;
        name = "NA";
    }
    
    public Patient(int a,String b) {
        
        id = a;
        name = b;
    }
    
    public void print() {
      
        System.out.println("Patient ID : " + id );
        System.out.println("Patient Name : " + name);
    }
    
    public static void main(String[] args) {
        
        Patient patient = new Patient();
        Patient patient1 = new Patient(1,"Sam");
        Admission admission = new Admission();
        patient.print();
        patient1.print();
        System.out.println("Admission Type : " + admission.type);
        System.out.println("Admission Date : " + admission.date);
        System.out.println("Admission Purpose : " + admission.purpose);
    }
}
