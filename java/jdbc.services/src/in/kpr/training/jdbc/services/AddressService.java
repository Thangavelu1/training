/*
Requirement:
	To perform the CRUD operation of the Address.

Entity:
	1.Address
    2.AddressService
    3.AppException
    4.ErrorCode

Function declaration:
	public long create(Address address) {}
    public Address read(long id) {}
    public ArrayList<Address> readAll() {}
    public void update(Address address) {}
    public void delete(long id) {}
    public ArrayList<Address> search(String street, String city, String postalCode) {}
    public Address readAddress(ResultSet result) {}
Jobs To Be Done:
    1. Create a Address.
    2. Read a record in the Address.
    3. Read all the record in the addresses.
    4. Update an Address.
    5. Delete an Address.
    6. Search the address which contains given data.
    7. Perform common operations for read and readAll.
 */

package in.kpr.training.jdbc.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import in.kpr.training.jdbc.constants.QueryStatements;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;
import in.kpr.training.jdbc.model.Address;

public class AddressService {

    public long create(Address address) {

        int numberOfRowsAffected;

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        }  

        try(PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatements.CREATE_ADDRESS_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setInt(3, address.getPostalCode());
            numberOfRowsAffected = ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();

            if (numberOfRowsAffected == 0 || !resultSet.next()) {
                throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED);
            }
            return resultSet.getLong("GENERATED_KEY");
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED, e);
        }
    }
    //change
    public Address read(long id) {

        Address address = null;

        try(PreparedStatement ps = ConnectionService.get()
                    .prepareStatement(QueryStatements.READ_ADDRESS_QUERY)) {
            
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                address = readAddress(result);
            }
            return address;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }

    public ArrayList<Address> readAll() {

        Address address = null;
        ArrayList<Address> addresses = new ArrayList<>();

        try(PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatements.READALL_ADDRESS_QUERY)) {
            
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                address = readAddress(result);
                addresses.add(address);
            }
            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }

    }

    public void update(Address address) {

        int numberOfRowsAffected = 0;

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        } 
        try(PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatements.UPDATE_ADDRESS_QUERY)) {

            ps.setString(1, address.getStreet());// create method - parameter ps, address
            ps.setString(2, address.getCity());
            ps.setInt(3, address.getPostalCode());
            ps.setLong(4, address.getId());
            numberOfRowsAffected = ps.executeUpdate();

            if (numberOfRowsAffected == 0) {
                throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED, e);
        }
    }

    public void delete(long id) {
        
        try(PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatements.DELETE_ADDRESS_QUERY)) {
            
            ps.setLong(1, id);

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED, e);
        }
    }

    public ArrayList<Address> search(String street, String city, String postalCode) {

        Address address = null;
        ArrayList<Address> addresses = new ArrayList<>();

        try(PreparedStatement ps = ConnectionService.get()
                .prepareStatement(QueryStatements.ADDRESS_SEARCH)) {
            
            ps.setString(1, "%" + street + "%");
            ps.setString(2, "%" + city + "%");
            ps.setString(3, "%" + postalCode + "%");
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                address = readAddress(resultSet);
                addresses.add(address);
            }
            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.SEARCHING_ADDRESS_FAILED, e);
        }

    }

    public Address readAddress(ResultSet result) {
        Address address = null;
        try {
            address = new Address(result.getString("street"), result.getString("city"),
                    result.getInt("postal_code"));// get from constants
            address.setId(result.getLong("id"));
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
        return address;
    }

    public long getAddressId(Address address, Connection con) {
        
        ResultSet resultSet;

        try(PreparedStatement ps = con.prepareStatement(QueryStatements.ADDRESS_UNIQUE)) {

            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setLong(3, address.getPostalCode());
            resultSet = ps.executeQuery();
            if (!resultSet.next()) {
                throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS);
            }
            return resultSet.getLong("id");

        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS, e);
        }
    }

}
