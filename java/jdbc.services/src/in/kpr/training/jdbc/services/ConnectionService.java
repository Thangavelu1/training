/*
Requirement:
    To create a connection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void init() 
    public void release() 
    
Jobs to be done:
    
    1. Declare Connection con.
    2. Declare init method.
    	2.1 Load db.properties file to a properties object.
        2.2 Establish the connection using sql driver and store it as con.
    3. Declare get method.
        3.1 Return connection object.
    4. Declare release method.
        4.1 Close the Connection of the server.
        4.2 Close the PreparedStatement of the query.
    5. Declare commit method.
        5.1 Commit the changes made after execution of query.
    6. Declare rollback method.
        6.1 Rollback the changes made by executing the query.

Pseudo code:
class ConnectionService {
    
    public Connection con;

    public void init() {

        Properties properties = new Properties();
        
        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            con = DriverManager.getConnection(properties.getProperty("URL"),
                    properties.getProperty("User Name"), properties.getProperty("Password"));
            con.setAutoCommit(false);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 

    }
    
    public static Connection get() {
        
        return con;
    }

    public void release() {
        try {
            con.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
    
    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
}

*/

package in.kpr.training.jdbc.services;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import in.kpr.training.jdbc.constants.Constants;
import in.kpr.training.jdbc.exception.AppException;
import in.kpr.training.jdbc.exception.ErrorCode;

@SuppressWarnings("unused")
public class ConnectionService extends ThreadPoolExecutor{

    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    
    public ConnectionService() {
        super(Constants.MAX_THREAD, Constants.MAX_THREAD, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }
    
    public void init() {

        Properties properties = new Properties();

        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            Connection con = DriverManager.getConnection(properties.getProperty(Constants.URL), 
                    properties.getProperty(Constants.USER_NAME), 
                    properties.getProperty(Constants.PASSWORD));
            con.setAutoCommit(false);
            
            threadLocal.set(con);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILED, e);
        } 
    }
    
    public static Connection get() {
        return threadLocal.get();
    }
    
    public static void release() {
        
        try {
            get().close();
            threadLocal.remove();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILED_TO_CLOSE, e);
        }
    }

    public static void commit() {
        
        try {
            get().commit();
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
    public static void rollback() {
        
        try {
            get().rollback();
        } catch (Exception e1) {
            throw new AppException(ErrorCode.FAILED_TO_ROLLBACK);
        }
    }
    
    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        init();
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        release();
    }
}